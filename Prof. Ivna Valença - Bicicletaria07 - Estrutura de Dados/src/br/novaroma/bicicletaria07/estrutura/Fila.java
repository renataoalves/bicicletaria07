package br.novaroma.bicicletaria07.estrutura;

import br.novaroma.bicicletaria07.bancodedados.SingletonDAO;
import br.novaroma.bicicletaria07.excecao.FilaCheiaExcecao;
import br.novaroma.bicicletaria07.excecao.FilaVaziaExcecao;
import br.novaroma.bicicletaria07.model.Servico;

public abstract class Fila {
	
	private int qnt, primeiro, ultimo;
	private static Servico[] array;
	private String servico;
	
	public Fila(int tamanho, String servico){
		setPrimeiro(0);
		setUltimo(0);
		array = new Servico[tamanho];
		this.servico = servico;
	}
	
	public void enfileirar(Servico servico){
		try{
			if(!filaCheia()){
				SingletonDAO.getInstancia().adicionarServicoEmFila(servico);
				array[ultimo] = servico;
				qnt++;
				if(ultimo >= array.length)
					ultimo = 0;
				else
					ultimo++;
			} else
				throw new FilaCheiaExcecao(getServico());
		} catch (FilaCheiaExcecao e){ }
	}
	
	public void desenfileirar(){
		try {
			if(!estaVazio()){
				SingletonDAO.getInstancia().finalizarServicoEmFila(array[primeiro]);
				array[primeiro] = null;
				qnt--;
				if(primeiro >= array.length-1)
					primeiro = 0;
				else
					primeiro++;
			} else
				throw new FilaVaziaExcecao(getServico());
		} catch(FilaVaziaExcecao e){ }
	}
	
	public Servico[] exibir(){
		return SingletonDAO.getInstancia().buscarServicoEmFila(getServico());
	}
	
	public void editar(Servico servico){
		if(!estaVazio())
			SingletonDAO.getInstancia().alterarServicoEmFila(servico);
	}
	
	public void retirar(Servico servico){
		if(!estaVazio())
			SingletonDAO.getInstancia().finalizarServicoEmFila(servico);
	}
	
	private boolean estaVazio(){
		return getQnt()==0;
	}
	private boolean filaCheia(){
		return getQnt()>array.length-1;
	}
	
	public int getQnt() {
		return qnt;
	}
	public String getServico(){
		return servico;
	}
	private void setPrimeiro(int primeiro) {
		this.primeiro = primeiro;
	}
	private void setUltimo(int ultimo) {
		this.ultimo = ultimo;
	}

}