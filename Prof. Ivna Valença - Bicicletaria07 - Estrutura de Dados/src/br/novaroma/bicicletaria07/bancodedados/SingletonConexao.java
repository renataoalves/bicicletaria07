package br.novaroma.bicicletaria07.bancodedados;

public class SingletonConexao {

	private static Conexao dao = null;
	
	public static Conexao getInstancia(){
		if(dao == null)
			dao = new Conexao();
		return dao;
	}
	
}
