package br.novaroma.bicicletaria07.bancodedados;

import java.time.LocalDate;

import br.novaroma.bicicletaria07.enums.SexoEnum;
import br.novaroma.bicicletaria07.enums.TipoFuncionarioEnum;
import br.novaroma.bicicletaria07.model.ClienteFisico;
import br.novaroma.bicicletaria07.model.ClienteJuridico;
import br.novaroma.bicicletaria07.model.Contato;
import br.novaroma.bicicletaria07.model.Endereco;
import br.novaroma.bicicletaria07.model.Fornecedor;
import br.novaroma.bicicletaria07.model.Funcionario;
import br.novaroma.bicicletaria07.model.Servico;

public class DAO {
	
	public void adicionarClienteF(ClienteFisico cf){}
	public void adicionarClienteJ(ClienteJuridico cj){}
	public void adicionarFornecedor(Fornecedor f){}
	public void adicionarFuncionario(Funcionario f){}
	
	public void alterarClienteF(long id, String nome, String cpf, SexoEnum sexo, LocalDate date,
			Contato contato, Endereco endereco){
	}
	public void alterarClienteJ(long id, String razao, String cnpj,
			Contato contato, Endereco endereco){
	}
	public void alterarFornecedor(long id, String razao, String cnpj,
			Contato contato, Endereco endereco){
	}
	public void alterarFuncionario(long id, String nome, String cpf, SexoEnum sexo, LocalDate date,
			Contato contato, Endereco endereco){
	}
	
	public ClienteFisico buscarClienteF(long id, String nome, String cpf,
			SexoEnum sexo, LocalDate dataIni, LocalDate dataFim,
			Contato contato, Endereco endereco){
		ClienteFisico cf = null;
		return cf;
	}
	public ClienteJuridico buscarClienteJ(long id, String razaoSocial,
			String cnpj, Contato contato, Endereco endereco){
		ClienteJuridico cj = null;
		return cj;
	}
	public Fornecedor buscarFornecedor(long id, String razaoSocial, String cnpj,
			Contato contato, Endereco endereco){
		Fornecedor f = null;
		return f;
	}
	public Funcionario buscarFuncionario(long id, String login, String senha, String nome, String cpf,
			SexoEnum sexo, LocalDate dataIni, LocalDate dataFim, Contato contato, Endereco endereco,
			TipoFuncionarioEnum tipoFuncionario, String emailEmpresarial){
		Funcionario f = null;
		return f;
	}
	
	public void excluirClienteF(long id){}
	public void excluirClienteJ(long id){}
	public void excluirFornecedor(long id){}
	public void excluirFuncionario(long id){}

	public void adicionarServicoEmFila(Servico servico){
	}
	public void alterarServicoEmFila(Servico servico){
	}
	public Servico[] buscarServicoEmFila(String tipoServico){
		return null;
	}
	public void finalizarServicoEmFila(Servico servico){
	}
}