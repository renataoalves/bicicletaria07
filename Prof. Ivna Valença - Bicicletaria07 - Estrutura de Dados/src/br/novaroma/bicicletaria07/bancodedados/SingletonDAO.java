package br.novaroma.bicicletaria07.bancodedados;

public class SingletonDAO {
	
	private static DAO dao = null;
	
	public static DAO getInstancia(){
		if(dao == null)
			dao = new DAO();
		return dao;
	}

}
