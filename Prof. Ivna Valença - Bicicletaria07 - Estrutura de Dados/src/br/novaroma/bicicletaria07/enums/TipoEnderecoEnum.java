package br.novaroma.bicicletaria07.enums;

public enum TipoEnderecoEnum {
	APARTAMENTO,
	CASA,
	SOBRADO,
	ANEXO,
	COMERCIAL,
	OUTRO;
}