package br.novaroma.bicicletaria07.enums;

public enum TipoFuncionarioEnum {
	ADM,
	VENDEDOR,
	MECANICO,
	PINTOR;
}