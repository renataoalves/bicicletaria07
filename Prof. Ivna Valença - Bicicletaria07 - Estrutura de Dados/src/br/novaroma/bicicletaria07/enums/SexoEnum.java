package br.novaroma.bicicletaria07.enums;

public enum SexoEnum {
	MASCULINO,
	FEMININO;
}