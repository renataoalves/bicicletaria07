package br.novaroma.bicicletaria07.enums;

public enum StatusServicoEnum {
	AGUARDO,
	INICIADO,
	FINALIZADO,
	CANCELADO
}
