package br.novaroma.bicicletaria07.controller;

import javax.swing.JOptionPane;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

import br.novaroma.bicicletaria07.componente.BarraDeMenu;
import br.novaroma.bicicletaria07.model.Funcionario;
import br.novaroma.bicicletaria07.view.ViewLogin;

public class EnviaEmailSuporte {
	
	// M�todo para envio de email para contata��o da equipe de suporte
	public static void enviarEmail(String email, String problema, String descricaoDoProblema, String usuario, String identificador){
		try {
			SimpleEmail emailS = new SimpleEmail();
			
	        emailS.setDebug(true); // DEBUGA
	        emailS.setHostName("smtp.gmail.com");
	        emailS.setAuthentication("atendimento.bicicletaria07@gmail.com", "bicicletariapernambucana"); // AUTENTICA��O DE EMAIL - EMPRESARIAL ATENDIMENTO
	        
	        emailS.setSSL(true);
	        
	        emailS.addTo("suporte.bicicletaria07@gmail.com", "SUPORTE AO USU�RIO");// EMAIL DA EQUIPE DE SUPORTE - EMPRESARIAL SUPORTE
	        
        	emailS.setFrom(email, pegarLoginEmailDeFuncionario(usuario, email, identificador).trim()); // EMAIL DO CLIENTE
	        
	        emailS.setSubject("SUPORTE AO USU�RIO - BICILETARIA_07 - ERRO: "+ problema); // T�TULO DO EMAIL
	        emailS.setMsg("\n\n"+descricaoDoProblema); // MENSAGEM - DESCRI��O DO PROBLEMA
	        
	        emailS.send(); // ENVIA O EMAIL
	        
	        ControleThreadCarregando.enquanto = false;
	        
	        // Tratamento para quando for enviado o email. Se o jframe foi instanciado pela tela de login, fecha. Se n�o, foi instanciado pela barra de menu e fecha 
	        try{
	        	ViewLogin.suporte.dispose();
	        } catch (NullPointerException e){
	        	BarraDeMenu.suporte.dispose();
	        }
	        
	        JOptionPane.showMessageDialog(null, "Nossa equipe de suporte acaba de receber seu e-mail."
												+ "\nPor favor fique no aguardo para que possamos lhe contatar para no caso de algum esclarecimento a mais",
												"SUPORTE ALERTADO", 1);
	        
        } catch (EmailException e) {
        	System.out.println(e.getMessage());
        	ControleThreadCarregando.enquanto = false;
        	JOptionPane.showMessageDialog(null, "OCORREU UM ERRO E POR ISSO N�O FOI POSS�VEL COMPLETAR O ENVIO DE EMAIL PARA CONTATA��O DE NOSSA EQUIPE DE SUPORTE."
												+ "\n\t 1. Verifique sua conex�o;"
												+ "\n\t 2. Verifique se o email informado est� correto;"
		    									+ "\n SE NADA FUNCIONAR, CONTATE-NOS PELO TELEFONE (96) 6969-6969", "ERRO NO ENVIO DO EMAIL", 0);
        }
	}
	
	// M�todo para buscar o email do funcionario que estiver logado no momento ou para buscar o login de acordo com email empresarial informado na tela de login 
	public static String pegarLoginEmailDeFuncionario(String usuario, String email, String identificador){
		String nomesFuncionarios[] = ControleLista.listarTodosFuncionarios();
		Funcionario funcionarios[] = new Funcionario [nomesFuncionarios.length];
		
		if(identificador.trim().equals("logado")){
			for(int x=0; x<nomesFuncionarios.length; x++){
				funcionarios[x] = ControleLista.buscarFuncionario(nomesFuncionarios[x]);
				if(funcionarios[x].getLogin().equals(usuario.trim()))
					return funcionarios[x].getContato().getEmail();
			}
			return "NAO ENCONTRADO EMAIL";
		} else {
			for(int x=0; x<nomesFuncionarios.length; x++){
				funcionarios[x] = ControleLista.buscarFuncionario(nomesFuncionarios[x]);
				System.out.println("buscando "+funcionarios[x].getEmailEmpresarial());
				System.out.println("buscando por "+email);
				if(funcionarios[x].getEmailEmpresarial().equals(email.trim()))
					return funcionarios[x].getLogin();
			}
			return "NAO ENCONTRADO LOGIN";
		}
	}
	
}