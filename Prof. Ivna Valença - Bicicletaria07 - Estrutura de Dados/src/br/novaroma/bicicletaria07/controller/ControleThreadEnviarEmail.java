package br.novaroma.bicicletaria07.controller;

public class ControleThreadEnviarEmail implements Runnable {

	private static Thread thread;
	private String email = "", problema = "", descricao = "", usuario = "", identificador = "";

	public void carregar(String email, String problema, String descricao, String usuario, String identificador){
		try {
			this.email = email;
			this.problema = problema;
			this.descricao = descricao;
			this.usuario = usuario;
			this.identificador = identificador;
			thread = new Thread(this);
			
			thread.start();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void run(){
		EnviaEmailSuporte.enviarEmail(email, problema, descricao, usuario, identificador);
		carregou();
	}
	
	public void carregou(){
		thread.stop();
	}
	
}