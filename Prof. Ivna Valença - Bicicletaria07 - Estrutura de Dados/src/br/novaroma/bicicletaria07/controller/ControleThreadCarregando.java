package br.novaroma.bicicletaria07.controller;

import javax.swing.JFrame;

import br.novaroma.bicicletaria07.view.SplashCarregando;

public class ControleThreadCarregando implements Runnable {
	
	protected static boolean enquanto;
	private static Thread thread;
	private SplashCarregando carregando;

	public void carregar(){
		try {
			thread = new Thread(this);
			thread.start();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void run(){
		try {
			enquanto = true;
			carregando = new SplashCarregando(new JFrame(), "Aguarde! Em andamento...");
			while(enquanto)
				Thread.sleep(1000);
			carregou();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void carregou(){
		carregando.dispose();
		thread.stop();
	}
	
}