package br.novaroma.bicicletaria07.controller;

import java.time.LocalDate;

import br.novaroma.bicicletaria07.bancodedados.SingletonDAO;
import br.novaroma.bicicletaria07.enums.SexoEnum;
import br.novaroma.bicicletaria07.enums.TipoFuncionarioEnum;
import br.novaroma.bicicletaria07.excecao.NaoEncontradoExcecao;
import br.novaroma.bicicletaria07.model.*;

public class ControleLista {
	
	public static void adicionaClienteFisico(String nome, String cpf, SexoEnum sexo, LocalDate dataNascimento,
			Contato contato, Endereco endereco){
		SingletonDAO.getInstancia().adicionarClienteF(new ClienteFisico(nome, cpf, sexo, dataNascimento, contato, endereco));
	}
	public static void adicionaClienteJuridico(String razaoSocial, String cnpj,
			Contato contato, Endereco endereco){
		SingletonDAO.getInstancia().adicionarClienteJ(new ClienteJuridico(razaoSocial, cnpj, contato, endereco));
	}
	public static void adicionaFornecedor(String razaoSocial, String cnpj,
			Contato contato, Endereco endereco){
		SingletonDAO.getInstancia().adicionarFornecedor(new Fornecedor(razaoSocial, cnpj, contato, endereco));
	}
	public static void adicionaFuncionario(String nome, String cpf, SexoEnum sexo, LocalDate dataNascimento,
			ContatoFuncionario contato, Endereco endereco,
			TipoFuncionarioEnum tipoFuncionario, String login, String senha){
		SingletonDAO.getInstancia().adicionarFuncionario(new Funcionario(login, senha, nome, cpf, sexo, dataNascimento, contato, endereco, tipoFuncionario));
	}
	
	public static Funcionario validarFuncionario(String login, String senha){
		senha = EncriptaSenha.encriptar(senha);
		Funcionario f = SingletonDAO.getInstancia().buscarFuncionario(0, login, senha, null, null, null, null, null, null, null, null, null);
		return (f.getLogin().equals(login) && f.getSenha().equals(senha) && f != null) ? f : null;
	}
	
	public static boolean identificarSeLoginExiste(String login){
		try {
			buscarFuncionario(0, login, null, null, null, null, null, null, null, null, null, null);
			return true;
		} catch (NaoEncontradoExcecao e) {
			return false;
		}
	}
	
	public static ClienteFisico buscarClienteFisico(long id, String nome, String cpf,
			SexoEnum sexo, LocalDate dataIni, LocalDate dataFim,
			Contato contato, Endereco endereco) throws NaoEncontradoExcecao {
		ClienteFisico cf = SingletonDAO.getInstancia().buscarClienteF(id, nome, cpf, sexo,
				dataIni, dataFim, contato, endereco);
		if(cf == null)
			throw new NaoEncontradoExcecao("Cliente F�sico "+nome+" n�o encontrado!");
		return cf;
	}
	public static ClienteJuridico buscarClienteJuridico(long id, String razaoSocial,
			String cnpj, Contato contato, Endereco endereco) throws NaoEncontradoExcecao {
		ClienteJuridico cj = SingletonDAO.getInstancia().buscarClienteJ(id, razaoSocial, cnpj,
				contato, endereco);
		if(cj == null)
			throw new NaoEncontradoExcecao("Cliente Jur�dico "+razaoSocial+" n�o encontrado!");
		return cj;
	}
	public static Fornecedor buscarFornecedor(long id, String razaoSocial, String cnpj,
			Contato contato, Endereco endereco) throws NaoEncontradoExcecao {
		Fornecedor f = SingletonDAO.getInstancia().buscarFornecedor(id, razaoSocial, cnpj, contato, endereco);
		if(f == null)
			throw new NaoEncontradoExcecao("Fornecedor "+razaoSocial+" n�o encontrado!");
		return f;
	}
	public static Funcionario buscarFuncionario(long id, String login, String senha, String nome, String cpf,
			SexoEnum sexo, LocalDate dataIni, LocalDate dataFim, Contato contato, Endereco endereco,
			TipoFuncionarioEnum tipoFuncionario, String emailEmpresarial) throws NaoEncontradoExcecao {
		Funcionario f = SingletonDAO.getInstancia().buscarFuncionario(id, login, senha, nome, cpf, sexo,
				dataIni, dataFim, contato, endereco, tipoFuncionario, emailEmpresarial);
		if(f == null)
			throw new NaoEncontradoExcecao("Funcion�rio "+nome+" n�o encontrado!");
		return f;
	}
	
	public static void alterarClienteFisico(long id, String nome, String cpf, SexoEnum sexo, LocalDate date, Contato contato, Endereco endereco){
		SingletonDAO.getInstancia().alterarClienteF(id, nome, cpf, sexo, date, contato, endereco);
	}
	public static void alterarClienteJuridico(long id, String razao, String cnpj, Contato contato, Endereco endereco){
		SingletonDAO.getInstancia().alterarClienteJ(id, razao, cnpj, contato, endereco);
	}
	public static void alterarFornecedor(long id, String razao, String cnpj, Contato contato, Endereco endereco){
		SingletonDAO.getInstancia().alterarFornecedor(id, razao, cnpj, contato, endereco);
	}
	public static void alterarFuncionario(long id, String nome, String cpf, SexoEnum sexo, LocalDate date, Contato contato, Endereco endereco){
		SingletonDAO.getInstancia().alterarFuncionario(id, nome, cpf, sexo, date, contato, endereco);
	}
	
	public static void excluirClienteFisico(long id){
		SingletonDAO.getInstancia().excluirClienteF(id);
	}
	public static void excluirClienteJuridico(long id){
		SingletonDAO.getInstancia().excluirClienteJ(id);
	}
	public static void excluirFornecedor(long id){
		SingletonDAO.getInstancia().excluirFornecedor(id);
	}
	public static void excluirFuncionario(long id){
		SingletonDAO.getInstancia().excluirFuncionario(id);
	}
	
}