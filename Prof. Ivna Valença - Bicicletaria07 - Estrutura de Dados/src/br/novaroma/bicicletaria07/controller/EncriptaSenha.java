package br.novaroma.bicicletaria07.controller;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class EncriptaSenha {
	
	// M�todo para encripar senha usando a t�cnica de criptografia MD5
	public static String encriptar(String senha) {
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return new BigInteger(1, md.digest(senha.getBytes())).toString(16);
	}

}