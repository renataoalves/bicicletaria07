package br.novaroma.bicicletaria07.controller;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.swing.JOptionPane;

import com.itextpdf.text.*;
import com.itextpdf.text.Font.*;
import com.itextpdf.text.pdf.*;

public class ImprimirPDF {
	
	// M�todo para impress�o de cadastro
	public static void imprimirCadastro(String usuario, String nomeRazao, String cpfCnpj, String sexo, String dataNascimento, String telFixo,
													 String telCelular, String email, String emailEmpresarial, String rua, String numero, String estado, String cidade,
													 String bairro, String cep, String tipoEndereco, String complemento,
													 boolean tipoRegistro, int tipo, String diretorio) throws Exception {
        Document doc = null;
        OutputStream os = null;
        
	    try {
	        //cria o documento tamanho A4, margens de 2,54cm
	        doc = new Document(PageSize.A4, 10, 10, 10, 10);
	
	        //cria a stream de sa�da
	        os = new FileOutputStream(diretorio);
	        
	        //associa a stream de sa�da ao diret�rio
	        PdfWriter.getInstance(doc, os);
	
	        //abre o documento
	        doc.open();
	        
	        PdfPTable cabecalho = new PdfPTable(new float[] { 0.7f, 0.3f });
	        cabecalho.setWidthPercentage(100.0f);
	        cabecalho.setHorizontalAlignment(Element.ALIGN_CENTER);
	        
	        PdfPTable tabelaGeral = new PdfPTable(1);
	        tabelaGeral.setWidthPercentage(100.0f);
	        tabelaGeral.setHorizontalAlignment(Element.ALIGN_CENTER);
	        
	        Font f = new Font(FontFamily.HELVETICA, 11, Font.BOLD);
	        
	        PdfPTable tabelaPrimeiraCelula = new PdfPTable(new float[] { 0.8f, 0.2f });
	        tabelaPrimeiraCelula.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
	        tabelaPrimeiraCelula.setHorizontalAlignment(Element.ALIGN_CENTER);
	        tabelaPrimeiraCelula.setWidthPercentage(100.0f);
	        
	        tabelaPrimeiraCelula.addCell("Usu�rio de impress�o: "+usuario);
	        tabelaPrimeiraCelula.addCell("");
	        
	        if(LocalDate.now().getDayOfMonth() < 10 && LocalDate.now().getMonthValue() < 10)
	        	tabelaPrimeiraCelula.addCell("Data: 0"+LocalDate.now().getDayOfMonth()+"/0"+LocalDate.now().getMonthValue()+"/"+LocalDate.now().getYear()+" ");
	        else if(LocalDate.now().getDayOfMonth() > 9 && LocalDate.now().getMonthValue() > 9)
	        	tabelaPrimeiraCelula.addCell("Data: "+LocalDate.now().getDayOfMonth()+"/"+LocalDate.now().getMonthValue()+"/"+LocalDate.now().getYear()+" ");
	        else if(LocalDate.now().getDayOfMonth() > 9 && LocalDate.now().getMonthValue() < 10)
	        	tabelaPrimeiraCelula.addCell("Data: "+LocalDate.now().getDayOfMonth()+"/0"+LocalDate.now().getMonthValue()+"/"+LocalDate.now().getYear()+" ");
	        else if(LocalDate.now().getDayOfMonth() < 9 && LocalDate.now().getMonthValue() > 10)
	        	tabelaPrimeiraCelula.addCell("Data: 0"+LocalDate.now().getDayOfMonth()+"/"+LocalDate.now().getMonthValue()+"/"+LocalDate.now().getYear()+" ");
	        
	        if(LocalDateTime.now().getHour() < 10 && LocalDateTime.now().getMinute() < 10)
	        	tabelaPrimeiraCelula.addCell("Hora: 0"+LocalDateTime.now().getHour()+":0"+LocalDateTime.now().getMinute());
	        else if(LocalDateTime.now().getHour() > 9 && LocalDateTime.now().getMinute() > 9)
	        	tabelaPrimeiraCelula.addCell("Hora: "+LocalDateTime.now().getHour()+":"+LocalDateTime.now().getMinute());
	        else if(LocalDateTime.now().getHour() > 9 && LocalDateTime.now().getMinute() < 10)
	        	tabelaPrimeiraCelula.addCell("Hora: "+LocalDateTime.now().getHour()+":0"+LocalDateTime.now().getMinute());
	        else if(LocalDateTime.now().getHour() < 9 && LocalDateTime.now().getMinute() > 10)
	        	tabelaPrimeiraCelula.addCell("Hora: 0"+LocalDateTime.now().getHour()+":"+LocalDateTime.now().getMinute());
	        
	        PdfPCell logoDaEmpresa = new PdfPCell(new Paragraph("LOGO DA EMPRESA", f));
	        logoDaEmpresa.setVerticalAlignment(Element.ALIGN_CENTER);
	        logoDaEmpresa.setHorizontalAlignment(Element.ALIGN_CENTER);
	        logoDaEmpresa.setColspan(1);
	        
	        PdfPCell header2;
	        if(tipo == 1){
	        	if(tipoRegistro)
		        	header2 = new PdfPCell(new Paragraph("REGISTRO DE CADASTRO DE CLIENTE F�SICO", f));
	        	else
		        	header2 = new PdfPCell(new Paragraph("REGISTRO DE CADASTRO DE FUNCION�RIO", f));
	        }else{
	        	if(tipoRegistro)
		        	header2 = new PdfPCell(new Paragraph("REGISTRO DE CADASTRO DE CLIENTE JUR�DICO", f));
	        	else
		        	header2 = new PdfPCell(new Paragraph("REGISTRO DE CADASTRO DE FORNECEDOR", f));
	        }
		        
        	header2.setVerticalAlignment(Element.ALIGN_CENTER);
	        header2.setHorizontalAlignment(Element.ALIGN_CENTER);
	        header2.setColspan(3);
	        
	        PdfPTable tblRegistros = new PdfPTable(1);
	        tblRegistros.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
	        tblRegistros.setHorizontalAlignment(Element.ALIGN_CENTER);
	        
	        tblRegistros.addCell(" ");
	        if(tipo==1){
		        tblRegistros.addCell(" NOME: "+nomeRazao);
		        tblRegistros.addCell(" CPF: "+cpfCnpj);
		        tblRegistros.addCell(" SEXO: "+sexo);
		        tblRegistros.addCell(" DATA DE NASCIMENTO: "+dataNascimento);
	        } else {
		        tblRegistros.addCell(" RAZ�O SOCIAL: "+nomeRazao);
		        tblRegistros.addCell(" CNPJ: "+cpfCnpj);
	        }
	        
	        tblRegistros.addCell(" ");
	        tblRegistros.addCell(" CONTATO ");
	        tblRegistros.addCell(" TELEFONE FIXO: "+telFixo);
	        tblRegistros.addCell(" TELEFONE CELULAR: "+telCelular);
	        tblRegistros.addCell(" EMAIL: "+email);
	        
	        if(tipo == 1)
	        	if(!tipoRegistro)
	        		tblRegistros.addCell(" EMAIL EMPRESARIAL: "+emailEmpresarial);
	        
	        tblRegistros.addCell(" ");
	        tblRegistros.addCell(" ENDERE�O ");
	        tblRegistros.addCell(" RUA: "+rua);
	        tblRegistros.addCell(" N�MERO: "+numero);
	        tblRegistros.addCell(" ESTADO: "+estado);
	        tblRegistros.addCell(" CIDADE: "+cidade);
	        tblRegistros.addCell(" BAIRRO: "+bairro);
	        tblRegistros.addCell(" CEP: "+cep);
	        tblRegistros.addCell(" TIPO DE ENDERE�O: "+tipoEndereco);
	        tblRegistros.addCell(" ");
	        tblRegistros.addCell(" COMPLEMENTO: "+complemento);
	        tblRegistros.addCell(" ");
	        
	        cabecalho.addCell(tabelaPrimeiraCelula);
	        cabecalho.addCell(logoDaEmpresa);
	        cabecalho.addCell(header2);
	        
	        tabelaGeral.addCell(cabecalho);
	        tabelaGeral.addCell(tblRegistros);
	        
	        doc.add(tabelaGeral);
	        
            if (doc != null) //fechamento do documento
                doc.close();
            
            if (os != null) //fechamento da stream de sa�da
               os.close();
            
            JOptionPane.showMessageDialog(null, "PDF salvo com sucesso!", "Sucesso!", 1);
	    } catch (NullPointerException e){
	    	JOptionPane.showMessageDialog(null, "Impress�o cancelada!", "Cancelado", 0);
        }
    }

}