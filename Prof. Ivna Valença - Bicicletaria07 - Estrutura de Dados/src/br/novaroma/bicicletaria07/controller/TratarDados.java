package br.novaroma.bicicletaria07.controller;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;

public class TratarDados {
	
	public String getCep(String cep){
		return cep.replace("-", "");
	}
	
	public String getCepFormatado(String cep){
		return cep.substring(0,5)+"-"+cep.substring(5);
	}
	
	public String getTelFixo(String tel){
		return tel.replace("(", "").replace(")", "").replace("-", ""); 
	}
	
	public String getTelFixoFormatado(String tel){
		return "("+tel.substring(0,2)+")"+tel.substring(2,6)+"-"+tel.substring(6); 
	}
	
	public String getTelCel(String tel){
		return tel.replace("(", "").replace(")", "").replace("-", ""); 
	}
	
	public String getTelCelFormatado(String tel){
		return "("+tel.substring(0,2)+")"+tel.substring(2,7)+"-"+tel.substring(7); 
	}
	
	public String getTextCpf(String cpf){
		return cpf.replace(".", "-").replace("-", "");
	}
	
	public String getTextCpfFormatado(String cpf){
		if(cpf.length() <= 2) return "-"+cpf;
		else return cpf.substring(0, 3)+"."+getTextCpfFormatado(cpf.substring(3));
	}
	
	public String getTextCnpj(String cnpj){
		return cnpj.replace(".", "").replace("-", "").replace("/", "");
	}
	
	public String getTextCnpjFormatado(String cnpj){
		return cnpj.substring(0,2)+"."+cnpj.substring(2,5)+"."+cnpj.substring(5,8)+"/"+cnpj.substring(8,12)+"-"+cnpj.substring(12);
	}
	
	public String getTextData(String data){
		return data.replace("/","");
	}
	
	public String getTextDataFormatado(String data){
		return data.substring(0,2)+"/"+data.substring(2,4)+"/"+data.substring(4);
	}
	
	public double getTextValor(String valor){
		return Double.parseDouble(valor.replace(".", "").replace(",", "."));
	}
	
	public static ImageIcon redimensionar(int x, int y, ImageIcon icon){
		Image image = icon.getImage();
		BufferedImage render = new BufferedImage (x, y, BufferedImage.TYPE_INT_RGB); // rederizador
	    Graphics2D g2 = render.createGraphics (); //criar objeto em 2d
	    g2.setRenderingHint (RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	    g2.drawImage(image, 0, 0, x, y, null); //cria imagem dimensionada
	    g2.dispose (); // limpa a imagem anterior para liberar outra
	    return new ImageIcon(render);
	 }
	
}
