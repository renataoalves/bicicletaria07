package br.novaroma.bicicletaria07.controller;

import br.novaroma.bicicletaria07.bancodedados.SingletonDAO;
import br.novaroma.bicicletaria07.enums.TipoServicoEnum;
import br.novaroma.bicicletaria07.estrutura.FilaLimpeza;
import br.novaroma.bicicletaria07.estrutura.FilaManutencao;
import br.novaroma.bicicletaria07.estrutura.FilaMontagem;
import br.novaroma.bicicletaria07.estrutura.FilaPintura;
import br.novaroma.bicicletaria07.model.Servico;

public class ControleFilas {
	
	private static final int tamanho = 20;
	private static FilaLimpeza filaLimpeza 				= new FilaLimpeza(tamanho);
	private static FilaManutencao filaManutencao 		= new FilaManutencao(tamanho);
	private static FilaMontagem filaMontagem 			= new FilaMontagem(tamanho);
	private static FilaPintura filaPintura					= new FilaPintura(tamanho);
	
	public static void enfileirar(Servico servico){
		SingletonDAO.getInstancia().adicionarServicoEmFila(servico);
		if(servico.getTipo() == TipoServicoEnum.LIMPEZA)
			filaLimpeza.enfileirar(servico);
		else if(servico.getTipo() == TipoServicoEnum.MANUTENCAO)
			filaManutencao.enfileirar(servico);
		else if(servico.getTipo() == TipoServicoEnum.MONTAGEM)
			filaMontagem.enfileirar(servico);
		else if(servico.getTipo() == TipoServicoEnum.PINTURA)
			filaPintura.enfileirar(servico);
	}
	
	public static void desenfileirar(Servico servico){
		SingletonDAO.getInstancia().finalizarServicoEmFila(servico);
		if(servico.getTipo() == TipoServicoEnum.LIMPEZA)
			filaLimpeza.desenfileirar();
		else if(servico.getTipo() == TipoServicoEnum.MANUTENCAO)
			filaManutencao.desenfileirar();
		else if(servico.getTipo() == TipoServicoEnum.MONTAGEM)
			filaMontagem.desenfileirar();
		else if(servico.getTipo() == TipoServicoEnum.PINTURA)
			filaPintura.desenfileirar();
	}
	
	public static Servico[] carregarFilaLimpeza(){
		return filaLimpeza.exibir();
	}
	public static Servico[] carregarFilaManutencao(){
		return filaManutencao.exibir();
	}
	public static Servico[] carregarFilaMontagem(){
		return filaMontagem.exibir();
	}
	public static Servico[] carregarFilaPintura(){
		return filaPintura.exibir();
	}
	
	public static void editar(Servico servico){
		SingletonDAO.getInstancia().alterarServicoEmFila(servico);
		if(servico.getTipo() == TipoServicoEnum.LIMPEZA)
			filaLimpeza.editar(servico);
		else if(servico.getTipo() == TipoServicoEnum.MANUTENCAO)
			filaManutencao.editar(servico);
		else if(servico.getTipo() == TipoServicoEnum.MONTAGEM)
			filaMontagem.editar(servico);
		else if(servico.getTipo() == TipoServicoEnum.PINTURA)
			filaPintura.editar(servico);
	}
	
}