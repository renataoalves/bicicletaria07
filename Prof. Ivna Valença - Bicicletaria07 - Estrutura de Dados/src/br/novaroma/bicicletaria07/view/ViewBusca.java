package br.novaroma.bicicletaria07.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import br.novaroma.bicicletaria07.componente.JButtonPattern;
import br.novaroma.bicicletaria07.componente.JIFramePattern;
import br.novaroma.bicicletaria07.componente.JPanelBusca;
import br.novaroma.bicicletaria07.model.ClienteFisico;
import br.novaroma.bicicletaria07.model.ClienteJuridico;
import br.novaroma.bicicletaria07.model.Fornecedor;
import br.novaroma.bicicletaria07.model.Funcionario;
import br.novaroma.bicicletaria07.model.Produto;
import br.novaroma.bicicletaria07.model.Servico;

public class ViewBusca extends JIFramePattern implements ActionListener {

	private static final long serialVersionUID = 1L;
	
	private JPanelBusca pn;
	private JButtonPattern btn;
	
	public ViewBusca(String titulo, Class<?> tipo) {
		super(titulo);
		
		btn = new JButtonPattern("Buscar");
		btn.addActionListener(this);
		if(tipo == ClienteFisico.class || tipo == ClienteJuridico.class || tipo == Fornecedor.class){
			add(btn).setBounds(590, 330, 100, 25);
			setSize(700, 390);
		} else if(tipo == Funcionario.class){
			add(btn).setBounds(590, 410, 100, 25);
			setSize(700, 470);
		} else if(tipo == Produto.class){
			add(btn).setBounds(395, 230, 100, 25);
			setSize(510, 290);
		} else if(tipo == Servico.class){
			add(btn).setBounds(387, 200, 100, 25);
			setSize(500, 263);
		}
		
		pn = new JPanelBusca(tipo);
		pn.removerObrigatoriedade();
		if(tipo == Produto.class || tipo == Servico.class)
			add(pn).setBounds(0, 0, 695, 350);
		else if(tipo == Funcionario.class)
			add(pn).setBounds(0, 0, 695, 600);
		else if(tipo == ClienteFisico.class || tipo == ClienteJuridico.class)
			add(pn).setBounds(0, 0, 695, 600);
		else if(tipo == Fornecedor.class)
			add(pn).setBounds(0, 0, 695, 600);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		dispose();
	}

	/*public static void main(String[] args) {
		JFramePadrao a = new JFramePadrao();
		a.add(new ViewBusca("TESTE", Funcionario.class));
		a.setVisible(true);
		a.setSize(800, 700);
		a.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		a.setLocationRelativeTo(null);
	}*/
	
}