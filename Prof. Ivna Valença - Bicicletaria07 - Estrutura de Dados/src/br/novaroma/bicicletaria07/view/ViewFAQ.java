package br.novaroma.bicicletaria07.view;

import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import br.novaroma.bicicletaria07.componente.JAreaPattern;
import br.novaroma.bicicletaria07.componente.JIFramePattern;
import br.novaroma.bicicletaria07.componente.JLabelPattern;

public class ViewFAQ extends JIFramePattern {
	
	private static final long serialVersionUID = 1L;

	private JAreaPattern txt;
	private JScrollPane scroll;
	
	public ViewFAQ(){
		super("Ajuda - Perguntas Frequentes (FAQs)");
		
		add(new JLabelPattern("Perguntas Frequentes (FAQs)")).setBounds(10, 11, 237, 23);
		
		txt = new JAreaPattern("+ Cadastrando um Cliente/Fornecedor/Funcion�rio:"
				+ "\n\tPara realizar um cadastro v� at� o menu <Arquivo>, <Cadastrar> e escolha entre <Cliente>, <Funcion�rio> e <Fornecedor>."
				+ "\n\tEscolhendo a op��o <Cliente>:"
				+ "\n\t- Escolha o identificador, CLIENTE F�SICO (pessoa) ou CLIENTE JUR�DICO (empresa)"
				+ "\n\t- Preencha o painel \"Informa��es Pessoais\" com os dados do cliente"
				+ "\n\t- Preencha o painel \"Contato\""
				+ "\n\t- Preencha o painel \"Endereco\""
				+ "\n\t- E clique em <Cadastrar>"
				+ "\n"
				+ "\n\tEscolhendo a op��o <Funcion�rio>:"
				+ "\n\t- Escolha o identificador, ADM, MEC�NICO ou VENDEDOR."
				+ "\n\t- Preencha o painel de acesso, inserindo um LOGIN n�o existente e uma SENHA"
				+ "\n\t- Preencha o painel \"Informa��es Pessoais\" com os dados do funcionario"
				+ "\n\t- Preencha o painel \"Contato\""
				+ "\n\t- Preencha o painel \"Endereco\""
				+ "\n\t- E clique em <Cadastrar>"
				+ "\n"
				+ "\n\tEscolhendo a op��o <Fornecedor>:"
				+ "\n\t- Escolha o identificador, PE�AS, BICICLETA ou ACESS�RIO"
				+ "\n\t- Preencha o painel \"Informa��es Pessoais\" com os dados do fornecedor"
				+ "\n\t- Preencha o painel \"Contato\""
				+ "\n\t- Preencha o painel \"Endereco\""
				+ "\n\t- E clique em <Cadastrar>"
				+ "\n"
				+ "\n+ Buscando, editando, excluindo e imprimindo registro de Cliente/Fornecedor/Funcion�rio:"
				+ "\n\tPara realizar uma busca por algum registros de cliente, fornecedor ou funcion�rio, dirija-se ao menu <Arquivo>, <Buscar>."
				+ "\n\t- Na nova janela que abrir, clique em um dos bot�es do painel \"Buscar por\". S�o os bot�es Cliente F., Cliente J., Fornecedor e Funcion�rio. Depois de escolhido algum bot�o, clique em <Buscar>."
				+ "\n\t- Um painel \"Lista de Registro\" ser� vis�vel logo abaixo do painel \"Buscar Por\". No mesmo se encontra todos os registros relacionados ao tipo de registro referente a busca. Se n�o houver aparecer� o painel mas com a mensagem \"N�o h� registro!\""
				+ "\n\t- Selecione o registro que deseja ver com mais detalhes, com 2 cliques ou selecionando e apertando a tecla enter."
				+ "\n\t- Ao lado ir� surgir outro painel com detalhes do registro escolhido."
				+ "\n"
				+ "\n\tPara realizar uma edi��o de cadastro, fa�a os mesmo passos anteriores e os pr�ximos:"
				+ "\n\t- Depois de estar vis�vel o painel com detalhamento do cadastro, h� um painel no canto superior direito. E no mesmo clique no bot�o <Editar>."
				+ "\n\t- O painel com as informa��es do cadastro ficar�o dispon�veis para altera��o."
				+ "\n\t- Edite"
				+ "\n\t- Depois terminado as altera��es necess�rias clique em <Salvar>, localizado no painel superior direito."
				+ "\n"
				+ "\n\tPara exclus�o de cadastro, fa�a os mesmo passos de busca e os pr�ximos:"
				+ "\n\t- Depois de estar vis�vel o painel com detalhamento do cadastro, h� um painel no canto superior direito. E no mesmo clique no bot�o <Excluir>."
				+ "\n"
				+ "\n\tPara impress�o de cadastro, fa�a os mesmo passos de busca e os pr�ximos:"
				+ "\n\t- Depois de estar vis�vel o painel com detalhamento do cadastro, h� um painel no canto superior direito. E no mesmo clique no bot�o <Imprimir>."
				+ "\n"
				+ "\n\tAdicionando, editando e atribuindo servi�o na fila de atendimento:"
				+ "\n\tPara adicionar um determinado servi�o a fila de espera. Dirija-se ao menu <Servi�o>, <Fila de Atendimento>."
				+ "\n\t- Na janela que dever� ser visualizada com t�tulo \"Atendimento - Servi�os\" ter� um painel de bot�es ao lado direito. No mesmo clique em \"Adicionar Servi�o\""
				+ "\n\t- Ficar� vis�vel outra janela. Informe o nome do cliente, o tipo de servi�o que o mesmo deseja e clique em <Adicionar>"
				+ "\n"
				+ "\n\tPara editar um determinado servi�o a fila de espera. Dirija-se ao menu <Servi�o>, <Fila de Atendimento>."
				+ "\n\t- Na janela que dever� ser visualizada com t�tulo \"Atendimento - Servi�os\" ter� 4 paineis, o que os diferencia s�o os tipos de servi�os. Escolha de qual servi�o deseja fazer a altera��o, na pr�xima janela que abrir�, edite e clique em <Salvar>."
				+ "\n"
				+ "\n\tPara atribuir um determinado servi�o da fila de espera. Dirija-se ao menu <Servi�o>, <Fila de Atendimento>."
				+ "\n\t- Na janela que dever� ser visualizada com t�tulo \"Atendimento - Servi�os\", no painel de bot�es ao lado direito clique em \"Editar\""
				+ "\n\t- Ficar� vis�vel outra janela. Clique em \"Atribuir Servi�o\". Depois informe a qual funcion�rio ser� atribu�do e clique em \"Atribuir Servi�o\"."
				+ "\n"
				+ "\n+ Evento"
				+ "\n\tPara adicionar participante ao sorteio, dirija-se ao menu <Eventos>, <Gerenciar Sorteio>, <Cadastrar Participante>."
				+ "\n\t- Na janela preencha o campo nome, clique em \"Gerar Senha\", preencha o telefone celular e clique em <Adicionar Participante>."
				+ "\n"
				+ "\n\tPara gerenciar os participante do sorteio, dirija-se ao menu <Eventos>, <Gerenciar Sorteio>, <Gerenciar Participante>."
				+ "\n\t- Na janela pode ser ordenada a lista de participantes pela senha ou alfabeticamente, clicando em dos bot�es localizados na parte superior do painel, consultado a quantidade participantes cadastrados, sortear um ganhador e buscar por um participante. Para buscar, informe o nome do participante ou a senha adquirida � ele, determinada quando estava sendo cadastrado."
				+ "\n"
				+ "\n+ Localizar postos pr�ximos da localidade atual da loja, dispon�veis para alugar bicicleta."
				+ "\n\tPara localizar os postos dispon�veis para alugar bicicleta, dirija-se ao menu <Postos>, <Mapa>."
				+ "\n\t- Na janela \"Mapa de Bicicletol�ndia\", � carregada o mapa. Ao clicar nos bot�es localizados no painel ao lado direito da tela, � calculado o menor caminho para o posto escolhido e carregado o mapa mostrando o percurso."
				+ "\n\tOBS: Ponto atual da loja � o primeiro selecionado. (v0)."
				+ "\n" + "\n+ Atalhos do Sistema:"
				+ "\n\t- Para abrir as op��es do menu:"
				+ "\n\t\t Arquivo: Alt + 1" + "\n\t\t Servi�o: Alt + 2"
				+ "\n\t\t Ajuda: Alt + 3" + "\n\t\t Evento: Alt + 4"
				+ "\n\t\t Postos: Alt + 5" + "\n"
				+ "\n\t- Na tela de login:"
				+ "\n\t\t Esqueci Senha: ALT + E"
				+ "\n\t\t Entrar: ALT + ENTER");
		txt.setEditable(false);
		
		scroll = new JScrollPane(txt);
		scroll.setBounds(10, 45, 900, 430);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		add(scroll);

		setSize(930, 520);
	}

	/*public static void main(String[] args) {
		JFramePadrao a = new JFramePadrao();
		a.add(new ViewFAQ());
		a.setVisible(true);
		a.setSize(930, 540);
		a.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		a.setLocationRelativeTo(null);
	}*/
	
}