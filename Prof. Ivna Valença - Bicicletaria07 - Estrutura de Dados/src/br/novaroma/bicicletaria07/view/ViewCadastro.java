package br.novaroma.bicicletaria07.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import br.novaroma.bicicletaria07.componente.JButtonPattern;
import br.novaroma.bicicletaria07.componente.JIFramePattern;
import br.novaroma.bicicletaria07.componente.JPanelCadastro;
import br.novaroma.bicicletaria07.model.ClienteFisico;
import br.novaroma.bicicletaria07.model.ClienteJuridico;
import br.novaroma.bicicletaria07.model.Fornecedor;
import br.novaroma.bicicletaria07.model.Funcionario;
import br.novaroma.bicicletaria07.model.Produto;

public class ViewCadastro extends JIFramePattern implements ActionListener {
	
	private static final long serialVersionUID = 1L;
	
	private JPanel panelCadastro;
	private JButtonPattern btn;

	public ViewCadastro(Class<?> tipo){
		super("");
		if(tipo == ClienteFisico.class)
			super.setTitle("Cadastro de Cliente F�sico");
		else if(tipo == ClienteJuridico.class)
			super.setTitle("Cadastro de Cliente Juridico");
		else if(tipo == Funcionario.class)
			super.setTitle("Cadastro de Funcion�rio");
		else if(tipo == Fornecedor.class)
			super.setTitle("Cadastro de Fornecedor");
		else if(tipo == Produto.class)
			super.setTitle("Cadastro de Produto");
		
		btn = new JButtonPattern("Cadastrar");
		btn.addActionListener(this);
		if(tipo == Funcionario.class)
			add(btn).setBounds(580, 400, 100, 25);
		else if(tipo == Fornecedor.class)
			add(btn).setBounds(580, 448, 100, 25);
		else if(tipo == Produto.class)
			add(btn).setBounds(380, 215, 100, 25);
		else
			add(btn).setBounds(580, 320, 100, 25);
		
		panelCadastro = new JPanelCadastro(tipo);
		if(tipo == Funcionario.class){
			add(panelCadastro).setBounds(0, 0, 690, 480);
			setSize(695, 465);
		}else if(tipo == Fornecedor.class){
			add(panelCadastro).setBounds(0, 0, 690, 525);
			setSize(695, 510);
		}else if(tipo == Produto.class){
			add(panelCadastro).setBounds(0, 0, 480, 208);
			setSize(490, 273);
		}else{
			add(panelCadastro).setBounds(0, 0, 690, 410);
			setSize(695, 385);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		dispose();
	}

	/*public static void main(String[] args) {
		JFrame a = new JFrame();
		a.add(new ViewCadastro(ClienteFisico.class));
		a.setVisible(true);
		a.setSize(941, 522);
		a.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		a.setLocationRelativeTo(null);
	}*/

}