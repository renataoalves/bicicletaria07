package br.novaroma.bicicletaria07.view;

import java.text.ParseException;

import javax.swing.JComboBox;

import br.novaroma.bicicletaria07.componente.JButtonPattern;
import br.novaroma.bicicletaria07.componente.JComboBoxTipos;
import br.novaroma.bicicletaria07.componente.JFieldCnpj;
import br.novaroma.bicicletaria07.componente.JFieldCpf;
import br.novaroma.bicicletaria07.componente.JFieldPadrao;
import br.novaroma.bicicletaria07.componente.JIFramePattern;
import br.novaroma.bicicletaria07.componente.JLabelPattern;
import br.novaroma.bicicletaria07.enums.TipoServicoEnum;

public class ViewServicoAdd extends JIFramePattern {

	private static final long serialVersionUID = 1L;

	private JFieldPadrao txtNome;
	private JFieldCpf txtCpf;
	private JFieldCnpj txtCnpj;
	private JComboBox<String> combo;
	private JButtonPattern btnBuscar, btnAddAtri, btnAdicionar;

	public ViewServicoAdd() {
		super("Adicionar Servi�o");
		setLayout(null);
		try {
			
			add(new JLabelPattern("Nome:")).setBounds(10, 10, 70, 15);

			txtNome = new JFieldPadrao();
			add(txtNome).setBounds(50, 10, 303, 20);
			
			add(new JLabelPattern("CPF:")).setBounds(20, 35, 70, 15);

			txtCpf = new JFieldCpf();
			add(txtCpf).setBounds(50, 35, 95, 20);
			
			txtCnpj = new JFieldCnpj();
			//add(txtCnpj).setBounds(50, 35, 130, 20);

			add(new JLabelPattern("Tipo de Servi�o:")).setBounds(10, 60, 101, 15);

			combo = new JComboBoxTipos(TipoServicoEnum.class);
			add(combo).setBounds(110, 60, 245, 20);

			btnBuscar = new JButtonPattern("Buscar Cliente");
			add(btnBuscar).setBounds(10, 90, 120, 25);
			
			btnAddAtri = new JButtonPattern("Add/Atri");
			add(btnAddAtri).setBounds(143, 90, 100, 25);
			
			btnAdicionar = new JButtonPattern("Adicionar");
			add(btnAdicionar).setBounds(255, 90, 100, 25);
			
			setSize(390, 195);
			
		} catch (ParseException e) { }
	}

	/*public static void main(String[] args) {
		JFrame a = new JFrame();
		a.add(new JIFrameServicoAdd());
		a.setSize(390, 195);
		a.setVisible(true);
		a.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		a.setLocationRelativeTo(null);
	}*/

}