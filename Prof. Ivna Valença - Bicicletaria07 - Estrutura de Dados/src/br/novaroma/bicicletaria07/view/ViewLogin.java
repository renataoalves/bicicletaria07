package br.novaroma.bicicletaria07.view;

import br.novaroma.bicicletaria07.componente.JButtonPattern;
import br.novaroma.bicicletaria07.componente.JFramePattern;
import br.novaroma.bicicletaria07.componente.JLabelPattern;
import br.novaroma.bicicletaria07.controller.TratarDados;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.ImageIcon;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class ViewLogin extends JFramePattern implements ActionListener, KeyListener {

	private static final long serialVersionUID = 1L;
	
	private JTextField txtLogin;
	private JPasswordField txtSenha;
	private JLabelPattern lblPlanoDeFundo;
	private JButtonPattern btnEntrar, btnEsqueceu;
	
	public ViewLogin() {
		super(null);
		
		lblPlanoDeFundo = new JLabelPattern("");
		lblPlanoDeFundo.setIcon(TratarDados.redimensionar(1200, 1000, new ImageIcon(getClass().getResource("/br/novaroma/bicicletaria07/imagem/planoFundo.jpg"))));
		
		add(new JLabelPattern("Informe seu login de acesso e senha:")).setBounds(70, 25, 254, 14);
		
		add(new JLabelPattern("Login:")).setBounds(75, 55, 55, 20);
		
		txtLogin = new JTextField();
		add(txtLogin).setBounds(115, 55, 195, 20);
		
		add(new JLabelPattern("Senha:")).setBounds(70, 80, 55, 17);
		
		txtSenha = new JPasswordField();
		add(txtSenha).setBounds(115, 80, 195, 20);
		
		btnEntrar = new JButtonPattern("Entrar");
		btnEntrar.addActionListener(this);
		add(btnEntrar).setBounds(210, 110, 100, 25);
		
		btnEsqueceu = new JButtonPattern("Esqueci senha");
		add(btnEsqueceu).setBounds(70, 110, 125, 25);
		
		add(lblPlanoDeFundo).setBounds(0, 0, 800, 160);
		
		txtLogin.addKeyListener(this);
		txtSenha.addKeyListener(this);
		
		setSize(800, 160);
		setLocationRelativeTo(null);
	}
	
	@Override
	public void criar() {
		setUndecorated(true);
		super.criar();
	}

	public void keyPressed(KeyEvent e) {}
	public void keyReleased(KeyEvent e) {}
	public void keyTyped(KeyEvent e) {}

	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == btnEntrar){
			new ViewPrincipal();
			dispose();
		}
	}
	
	/*public static void main(String[] args) {
		new ViewLogin();
	}*/

}