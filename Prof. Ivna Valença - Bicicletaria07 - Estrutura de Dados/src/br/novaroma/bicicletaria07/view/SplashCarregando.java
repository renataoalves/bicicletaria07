package br.novaroma.bicicletaria07.view;

import java.util.concurrent.TimeUnit;

import javax.swing.ImageIcon;

import br.novaroma.bicicletaria07.componente.JFramePattern;
import br.novaroma.bicicletaria07.componente.JLabelPattern;

public class SplashCarregando extends JFramePattern implements Runnable {
	
	private static final long serialVersionUID = 1L;
	
	private JLabelPattern lbl;
	private boolean enquanto = true;
	
	public SplashCarregando() {
        super("Carregando");
		
		lbl = new JLabelPattern("Carregando", 34);
		add(lbl).setBounds(95, 0, 250, 45);
		
		new Thread(this).start();
		
		add(new JLabelPattern(new ImageIcon(getClass().getResource("/br/novaroma/bicicletaria07/images/carregando.gif")))).setBounds(10, 10, 385, 200);
		
		setSize(390, 185);
		setLocationRelativeTo(null);
	}
	
	@Override
	public void criar() {
		setUndecorated(true);
		super.criar();
	}
	
	public void run() {
		try {
			while(enquanto){
				Thread.sleep(TimeUnit.SECONDS.toMillis(1));
				lbl.setText(lbl.getText()+".");
				lbl.setBounds(lbl.getBounds().x-5, lbl.getBounds().y, lbl.getBounds().width, lbl.getBounds().height);
				Thread.sleep(TimeUnit.SECONDS.toMillis(1));
				lbl.setText(lbl.getText()+".");
				lbl.setBounds(lbl.getBounds().x-5, lbl.getBounds().y, lbl.getBounds().width, lbl.getBounds().height);
				Thread.sleep(TimeUnit.SECONDS.toMillis(1));
				lbl.setText(lbl.getText()+".");
				lbl.setBounds(lbl.getBounds().x-5, lbl.getBounds().y, lbl.getBounds().width, lbl.getBounds().height);
				Thread.sleep(TimeUnit.SECONDS.toMillis(1));
				lbl.setText(lbl.getText().replace(".", ""));
				lbl.setBounds(95, lbl.getBounds().y, lbl.getBounds().width, lbl.getBounds().height);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		new SplashCarregando();
	}
	
}