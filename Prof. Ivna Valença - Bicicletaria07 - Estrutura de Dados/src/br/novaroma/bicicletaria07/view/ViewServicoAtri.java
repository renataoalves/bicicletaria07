package br.novaroma.bicicletaria07.view;

import java.text.ParseException;

import javax.swing.JComboBox;

import br.novaroma.bicicletaria07.componente.JButtonPattern;
import br.novaroma.bicicletaria07.componente.JFieldCpf;
import br.novaroma.bicicletaria07.componente.JFieldPadrao;
import br.novaroma.bicicletaria07.componente.JIFramePattern;
import br.novaroma.bicicletaria07.componente.JLabelPattern;

public class ViewServicoAtri extends JIFramePattern {
	
	private static final long serialVersionUID = 1L;
	
	private JFieldPadrao txtNome, txtTipo;
	private JFieldCpf txtCpf;
	private JButtonPattern btnAtribuir;
	private JComboBox<String> combo;
	
	public ViewServicoAtri() {
		super("Atribui��o de Servi�o");
		setLayout(null);
		try {
			
			add(new JLabelPattern("Nome:")).setBounds(10, 10, 70, 15);
			
			txtNome = new JFieldPadrao();
			add(txtNome).setBounds(50, 10, 303, 20);
			txtNome.setEditable(false);
			
			add(new JLabelPattern("CPF:")).setBounds(20, 35, 70, 15);
	
			txtCpf = new JFieldCpf();
			add(txtCpf).setBounds(50, 35, 95, 20);
			txtCpf.setEditable(false);
			
			add(new JLabelPattern("Tipo de Servi�o:")).setBounds(10, 60, 100, 15);
			
			txtTipo = new JFieldPadrao();
			add(txtTipo).setBounds(110, 60, 157, 20);
			txtTipo.setEditable(false);
			
			add(new JLabelPattern("Encarregado:")).setBounds(28, 85, 101, 15);;
			
			combo = new JComboBox<String>();
			combo.addItem("-- SELECIONE O ENCARREGADO --");
			add(combo).setBounds(110, 85, 242, 20);
			
			btnAtribuir = new JButtonPattern("Atribuir Servi�o");
			add(btnAtribuir).setBounds(222, 115, 130, 25);
			
			setSize(390, 225);
			
		} catch (ParseException e) { }
	}
	
	/*public static void main(String[] args) {
		JFrame a = new JFrame();
		a.add(new JIFrameServicoAtri());
		a.setSize(390, 225);
		a.setVisible(true);
		a.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		a.setLocationRelativeTo(null);
	}*/
	
}