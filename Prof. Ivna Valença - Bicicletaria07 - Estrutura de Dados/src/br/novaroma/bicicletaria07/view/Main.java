package br.novaroma.bicicletaria07.view;

import java.awt.EventQueue;

public class Main {
    
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new ViewLogin();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
}