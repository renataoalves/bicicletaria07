package br.novaroma.bicicletaria07.view;

import java.text.ParseException;

import javax.swing.JComboBox;

import br.novaroma.bicicletaria07.componente.JButtonPattern;
import br.novaroma.bicicletaria07.componente.JComboBoxTipos;
import br.novaroma.bicicletaria07.componente.JFieldCnpj;
import br.novaroma.bicicletaria07.componente.JFieldCpf;
import br.novaroma.bicicletaria07.componente.JFieldPadrao;
import br.novaroma.bicicletaria07.componente.JIFramePattern;
import br.novaroma.bicicletaria07.componente.JLabelPattern;
import br.novaroma.bicicletaria07.enums.TipoServicoEnum;

public class ViewServicoEdi extends JIFramePattern {
	
	private static final long serialVersionUID = 1L;
	
	private JFieldPadrao txtNome;
	private JFieldCpf txtCpf;
	private JFieldCnpj txtCnpj;
	private JComboBox<String> comboTipo;
	private JButtonPattern btnExcluir, btnSalvar;
	private JComboBox<String> comboEncarregado;

	public ViewServicoEdi() {
		super("Edi��o Servi�o");
		setLayout(null);
		try {
			
			add(new JLabelPattern("Nome:")).setBounds(10, 10, 70, 15);
	
			txtNome = new JFieldPadrao();
			add(txtNome).setBounds(50, 10, 303, 20);
			
			add(new JLabelPattern("CPF:")).setBounds(20, 35, 70, 15);
	
			txtCpf = new JFieldCpf();
			add(txtCpf).setBounds(50, 35, 95, 20);
			
			txtCnpj = new JFieldCnpj();
			//add(txtCnpj).setBounds(50, 35, 130, 20);
	
			add(new JLabelPattern("Tipo de Servi�o:")).setBounds(10, 60, 101, 15);
	
			comboTipo = new JComboBoxTipos(TipoServicoEnum.class);
			add(comboTipo).setBounds(110, 60, 245, 20);
			
			add(new JLabelPattern("Encarregado:")).setBounds(28, 85, 100, 20);
			
			comboEncarregado = new JComboBox<String>();
			comboEncarregado.addItem("-- SELECIONE O ENCARREGADO --");
			add(comboEncarregado).setBounds(110, 85, 245, 20);
			
			btnExcluir = new JButtonPattern("Excluir");
			add(btnExcluir).setBounds(10, 117, 100, 25);
			
			btnSalvar = new JButtonPattern("Salvar");
			add(btnSalvar).setBounds(255, 117, 100, 25);
			
			setSize(390, 225);

		} catch (ParseException e) { }
	}

	/*public static void main(String[] args) {
		JFrame a = new JFrame();
		a.add(new JIFrameServicoEdi());
		a.setSize(390, 225);
		a.setVisible(true);
		a.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		a.setLocationRelativeTo(null);
	}*/
	
}