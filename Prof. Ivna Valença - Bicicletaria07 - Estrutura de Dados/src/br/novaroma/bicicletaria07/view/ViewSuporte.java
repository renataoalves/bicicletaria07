package br.novaroma.bicicletaria07.view;

import br.novaroma.bicicletaria07.componente.JIFramePattern;
import br.novaroma.bicicletaria07.componente.JPanelSuporte;

public class ViewSuporte extends JIFramePattern {
	
	private static final long serialVersionUID = 1L;
	
	private JPanelSuporte panel;
	
	public ViewSuporte(String email, String identificador) {
		super("Contatação de Suporte");
		
		panel = new JPanelSuporte(email, identificador);
		add(panel);
		
		setSize(617, 430);
	}
	
	/*public static void main(String[] args) {
		JFramePadrao a = new JFramePadrao();
		a.add(new ViewSuporte("identificador", "email"));
		a.setVisible(true);
		a.setSize(630, 420);
		a.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		a.setLocationRelativeTo(null);
	}*/
	
}