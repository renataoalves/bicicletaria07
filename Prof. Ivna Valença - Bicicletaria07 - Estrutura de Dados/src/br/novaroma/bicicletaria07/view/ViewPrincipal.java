package br.novaroma.bicicletaria07.view;

import javax.swing.JFrame;

import br.novaroma.bicicletaria07.componente.BarraDeMenu;

public class ViewPrincipal extends BarraDeMenu {
	
	private static final long serialVersionUID = 1L;
	
	public ViewPrincipal() {
		setTitle("Bicicletaria07");
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		
	    /*Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
	    setSize(d.width, d.height);*/
		
		setSize(800, 700);
		setLocationRelativeTo(null);
	}
	
	//public static void main(String[] args) { new ViewPrincipal(); }

}