package br.novaroma.bicicletaria07.view;

import java.awt.Rectangle;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JScrollPane;

import br.novaroma.bicicletaria07.componente.JIFramePattern;

public class ViewBuscaRegistros extends JIFramePattern {
	
	private static final long serialVersionUID = 1L;

	private JScrollPane scroll;
	private JList<String> lista;
	private DefaultListModel<String> model;
	
	public ViewBuscaRegistros() {
		super("Resultado de sua busca");
		
		model = new DefaultListModel<String>();
		model.addElement("Renatoooooooooooo");
		model.addElement("Renatoooooooooooo");
		model.addElement("Renatoooooooooooo");
		model.addElement("Renatoooooooooooo");
		model.addElement("Renatoooooooooooo");
		model.addElement("Renatoooooooooooo");
		model.addElement("Renatoooooooooooo");
		model.addElement("Renatoooooooooooo");
		model.addElement("Renatoooooooooooo");
		model.addElement("Renatoooooooooooo");
		
		lista = new JList<String>(model);
		lista.setBounds(0, 0, 657, 500);
		lista.scrollRectToVisible(new Rectangle());
		
		scroll = new JScrollPane(lista, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		add(scroll).setBounds(0, 0, 700, 500);
	}
	
	/*public static void main(String[] args) {
		JFrame a = new JFrame();
		a.add(new ViewBuscaRegistros());
		a.setSize(700, 300);
		a.setVisible(true);
		a.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		a.setLocationRelativeTo(null);
	}*/

}