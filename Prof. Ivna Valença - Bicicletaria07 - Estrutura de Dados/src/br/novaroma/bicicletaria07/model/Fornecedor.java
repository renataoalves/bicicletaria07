package br.novaroma.bicicletaria07.model;

import java.util.ArrayList;

public class Fornecedor extends Empresa {
	
	private ArrayList<Produto> produto;

	// quando retornado do banco
	public Fornecedor(long id, String razaoSocial, String cnpj,
			Contato contato, Endereco endereco){
		super(id, razaoSocial, cnpj, contato, endereco);
	}
	
	// quando criado para inserir no banco
	public Fornecedor(String razaoSocial, String cnpj,
			Contato contato, Endereco endereco){
		super(razaoSocial, cnpj, contato, endereco);
	}
	
	public ArrayList<Produto> getProdutos() {
		return produto;
	}

	public void setProdutos(ArrayList<Produto> produtos) {
		this.produto = produtos;
	}

}