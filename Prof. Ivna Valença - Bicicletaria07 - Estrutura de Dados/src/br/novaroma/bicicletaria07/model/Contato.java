package br.novaroma.bicicletaria07.model;

public class Contato {
	
	private long id;
	private String telefoneFixo, celular, email;
	
	// quando retornado do banco
	public Contato(long id, String email, String celular){
		setId(id);
		setEmail(email);
		setCelular(celular);
	}
	
	// quando retornado do banco
	public Contato(long id, String email, String celular,
			String telefoneFixo){
		setId(id);
		setEmail(email);
		setCelular(celular);
		setTelefoneFixo(telefoneFixo);
	}
	
	// quando criado para inserir no banco
	public Contato(String email, String celular){
		setEmail(email);
		setCelular(celular);
	}

	// quando criado para inserir no banco
	public Contato(String email, String celular,
			String telefoneFixo){
		setEmail(email);
		setCelular(celular);
		setTelefoneFixo(telefoneFixo);
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTelefoneFixo() {
		return telefoneFixo;
	}

	public void setTelefoneFixo(String telefoneFixo) {
		this.telefoneFixo = telefoneFixo;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}