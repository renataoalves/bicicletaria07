package br.novaroma.bicicletaria07.model;

public class ClienteJuridico extends Empresa {
	
	// quando retornado do banco
	public ClienteJuridico(long id, String razaoSocial,
			String cnpj, Contato contato, Endereco endereco){
		super(id, razaoSocial, cnpj, contato, endereco);
	}
	
	// quando criado para inserir no banco
	public ClienteJuridico(String razaoSocial,
			String cnpj, Contato contato, Endereco endereco){
		super(razaoSocial, cnpj, contato, endereco);
	}

}