package br.novaroma.bicicletaria07.model;

import java.time.LocalDate;

import br.novaroma.bicicletaria07.enums.StatusServicoEnum;
import br.novaroma.bicicletaria07.enums.TipoServicoEnum;

public class Servico {
	
	private long id;
	private String cpfAnonimo, nome, descricao;
	private Pessoa clienteF;
	private Empresa clienteJ;
	private TipoServicoEnum tipo;
	private StatusServicoEnum status;
	private Funcionario fCriou, fAtribuiu, fFinalizou;
	private LocalDate dataIni, dataAtri, dataFim;
	
	// quando retornado do banco
	public Servico(long id, String cpfAnonimo, String nome, String descricao, TipoServicoEnum tipo, StatusServicoEnum status,
			Funcionario fCriou, LocalDate dataIni,
			Funcionario fAtribuiu, LocalDate dataAtri,
			Funcionario fFinalizou, LocalDate dataFim){
		setId(id);
		setCpfAnonimo(cpfAnonimo);
		setNome(nome);
		setDescricao(descricao);
		setTipo(tipo);
		setStatus(status);
		setfCriou(fCriou);
		setDataIni(dataIni);
		setfAtribuiu(fAtribuiu);
		setDataAtri(dataAtri);
		setfFinalizou(fFinalizou);
		setDataFim(dataFim);
	}

	// quando retornado do banco
	public Servico(long id, Pessoa cliente, String descricao, TipoServicoEnum tipo, StatusServicoEnum status,
			Funcionario fCriou, LocalDate dataIni,
			Funcionario fAtribuiu, LocalDate dataAtri,
			Funcionario fFinalizou, LocalDate dataFim){
		setId(id);
		setClienteF(cliente);
		setDescricao(descricao);
		setTipo(tipo);
		setStatus(status);
		setfCriou(fCriou);
		setDataIni(dataIni);
		setfAtribuiu(fAtribuiu);
		setDataAtri(dataAtri);
		setfFinalizou(fFinalizou);
		setDataFim(dataFim);
	}
	
	// quando retornado do banco
	public Servico(long id, Empresa cliente, String descricao, TipoServicoEnum tipo, StatusServicoEnum status,
			Funcionario fCriou, LocalDate dataIni,
			Funcionario fAtribuiu, LocalDate dataAtri,
			Funcionario fFinalizou, LocalDate dataFim){
		setId(id);
		setClienteJ(cliente);
		setDescricao(descricao);
		setTipo(tipo);
		setStatus(status);
		setfCriou(fCriou);
		setDataIni(dataIni);
		setfAtribuiu(fAtribuiu);
		setDataAtri(dataAtri);
		setfFinalizou(fFinalizou);
		setDataFim(dataFim);
	}
	
	// quando criado para inserir no banco
	public Servico(String cpfAnonimo, String nome, String descricao, TipoServicoEnum tipo, StatusServicoEnum status,
			Funcionario fCriou, LocalDate dataIni,
			Funcionario fAtribuiu, LocalDate dataAtri,
			Funcionario fFinalizou, LocalDate dataFim){
		setCpfAnonimo(cpfAnonimo);
		setNome(nome);
		setDescricao(descricao);
		setTipo(tipo);
		setStatus(status);
		setfCriou(fCriou);
		setDataIni(dataIni);
		setfAtribuiu(fAtribuiu);
		setDataAtri(dataAtri);
		setfFinalizou(fFinalizou);
		setDataFim(dataFim);
	}

	// quando retornado do banco
	public Servico(Pessoa cliente, String descricao, TipoServicoEnum tipo, StatusServicoEnum status,
			Funcionario fCriou, LocalDate dataIni,
			Funcionario fAtribuiu, LocalDate dataAtri,
			Funcionario fFinalizou, LocalDate dataFim){
		setClienteF(cliente);
		setDescricao(descricao);
		setTipo(tipo);
		setStatus(status);
		setfCriou(fCriou);
		setDataIni(dataIni);
		setfAtribuiu(fAtribuiu);
		setDataAtri(dataAtri);
		setfFinalizou(fFinalizou);
		setDataFim(dataFim);
	}
	
	// quando retornado do banco
	public Servico(Empresa cliente, String descricao, TipoServicoEnum tipo, StatusServicoEnum status,
			Funcionario fCriou, LocalDate dataIni,
			Funcionario fAtribuiu, LocalDate dataAtri,
			Funcionario fFinalizou, LocalDate dataFim){
		setClienteJ(cliente);
		setDescricao(descricao);
		setTipo(tipo);
		setStatus(status);
		setfCriou(fCriou);
		setDataIni(dataIni);
		setfAtribuiu(fAtribuiu);
		setDataAtri(dataAtri);
		setfFinalizou(fFinalizou);
		setDataFim(dataFim);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCpfAnonimo() {
		return cpfAnonimo;
	}

	public void setCpfAnonimo(String cpfAnonimo) {
		this.cpfAnonimo = cpfAnonimo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Pessoa getClienteF() {
		return clienteF;
	}

	public void setClienteF(Pessoa clienteF) {
		this.clienteF = clienteF;
	}

	public Empresa getClienteJ() {
		return clienteJ;
	}

	public void setClienteJ(Empresa clienteJ) {
		this.clienteJ = clienteJ;
	}

	public TipoServicoEnum getTipo() {
		return tipo;
	}

	public void setTipo(TipoServicoEnum tipo) {
		this.tipo = tipo;
	}

	public StatusServicoEnum getStatus() {
		return status;
	}

	public void setStatus(StatusServicoEnum status) {
		this.status = status;
	}

	public Funcionario getfCriou() {
		return fCriou;
	}

	public void setfCriou(Funcionario fCriou) {
		this.fCriou = fCriou;
	}

	public Funcionario getfAtribuiu() {
		return fAtribuiu;
	}

	public void setfAtribuiu(Funcionario fAtribuiu) {
		this.fAtribuiu = fAtribuiu;
	}

	public Funcionario getfFinalizou() {
		return fFinalizou;
	}

	public void setfFinalizou(Funcionario fFinalizou) {
		this.fFinalizou = fFinalizou;
	}

	public LocalDate getDataIni() {
		return dataIni;
	}

	public void setDataIni(LocalDate dataIni) {
		this.dataIni = dataIni;
	}

	public LocalDate getDataAtri() {
		return dataAtri;
	}

	public void setDataAtri(LocalDate dataAtri) {
		this.dataAtri = dataAtri;
	}

	public LocalDate getDataFim() {
		return dataFim;
	}

	public void setDataFim(LocalDate dataFim) {
		this.dataFim = dataFim;
	}

}