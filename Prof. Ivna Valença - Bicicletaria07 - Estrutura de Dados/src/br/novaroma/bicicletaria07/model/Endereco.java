package br.novaroma.bicicletaria07.model;

import br.novaroma.bicicletaria07.enums.TipoEnderecoEnum;

public class Endereco {
	
	private long id;
	private String cep, rua, numero, complemento, bairro, cidade, estado;
	private TipoEnderecoEnum tipoEndereco;

	// quando retornado do banco
	public Endereco(long id, String cep, String rua,
			String numero, String bairro, String cidade,
			String estado, TipoEnderecoEnum tipoEndereco){
		setId(id);
		setCep(cep);
		setRua(rua);
		setNumero(numero);
		setBairro(bairro);
		setCidade(cidade);
		setEstado(estado);
		setTipoEndereco(tipoEndereco);
	}
	
	// quando retornado do banco
	public Endereco(long id, String cep, String rua,
			String numero, String complemento, String bairro,
			String cidade, String estado, TipoEnderecoEnum tipoEndereco){
		setId(id);
		setCep(cep);
		setRua(rua);
		setNumero(numero);
		setBairro(bairro);
		setCidade(cidade);
		setEstado(estado);
		setComplemento(complemento);
		setTipoEndereco(tipoEndereco);
	}	

	// quando criado para inserir no banco
	public Endereco(String cep, String rua,
			String numero, String bairro, String cidade,
			String estado, TipoEnderecoEnum tipoEndereco){
		setCep(cep);
		setRua(rua);
		setNumero(numero);
		setBairro(bairro);
		setCidade(cidade);
		setEstado(estado);
		setTipoEndereco(tipoEndereco);
	}
	
	public Endereco(String cep, String rua,
			String numero, String complemento, String bairro,
			String cidade, String estado, TipoEnderecoEnum tipoEndereco){
		setCep(cep);
		setRua(rua);
		setNumero(numero);
		setBairro(bairro);
		setCidade(cidade);
		setEstado(estado);
		setComplemento(complemento);
		setTipoEndereco(tipoEndereco);
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getRua() {
		return rua;
	}

	public void setRua(String rua) {
		this.rua = rua;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	public TipoEnderecoEnum getTipoEndereco() {
		return tipoEndereco;
	}

	public void setTipoEndereco(TipoEnderecoEnum tipoEndereco) {
		this.tipoEndereco = tipoEndereco;
	}
	
}