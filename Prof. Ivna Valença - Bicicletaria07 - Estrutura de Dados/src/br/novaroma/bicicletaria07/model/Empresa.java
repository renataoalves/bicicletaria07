package br.novaroma.bicicletaria07.model;

public abstract class Empresa {
	
	private long id;
	private String razaoSocial, cnpj;
	private Contato contato;
	private Endereco endereco;
	
	// quando retornado do banco
	public Empresa(long id, String razaoSocial, String cnpj, Contato contato, Endereco endereco){
		setId(id);
		setRazaoSocial(razaoSocial);
		setCnpj(cnpj);
		setContato(contato);
		setEndereco(endereco);
	}
	
	// quando criado para inserir no banco
	public Empresa(String razaoSocial, String cnpj, Contato contato, Endereco endereco){
		setId(id);
		setRazaoSocial(razaoSocial);
		setCnpj(cnpj);
		setContato(contato);
		setEndereco(endereco);
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public Contato getContato() {
		return contato;
	}

	public void setContato(Contato contato) {
		this.contato = contato;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	
}