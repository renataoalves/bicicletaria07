package br.novaroma.bicicletaria07.model;

import java.time.LocalDate;

import br.novaroma.bicicletaria07.enums.SexoEnum;

public abstract class Pessoa {
	
	private long id;
	private String nome, cpf;
	private SexoEnum sexo;
	private LocalDate dataNascimento;
	private Endereco endereco;
	
	// quando retornado do banco
	public Pessoa(long id, String nome, String cpf, SexoEnum sexo, LocalDate dataNascimento, Endereco endereco){
		setId(id);
		setNome(nome);
		setCpf(cpf);
		setDataNascimento(dataNascimento);
		setEndereco(endereco);
	}
	
	// quando criado para inserir no banco
	public Pessoa(String nome, String cpf, SexoEnum sexo, LocalDate dataNascimento, Endereco endereco){
		setNome(nome);
		setCpf(cpf);
		setDataNascimento(dataNascimento);
		setEndereco(endereco);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public SexoEnum getSexo() {
		return sexo;
	}

	public void setSexo(SexoEnum sexo) {
		this.sexo = sexo;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

}