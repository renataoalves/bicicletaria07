package br.novaroma.bicicletaria07.model;

import java.util.ArrayList;

import br.novaroma.bicicletaria07.enums.TipoProdutoEnum;

public class Produto {
	
	private long id;
	private int quantidade;
	private String nomeProduto, descricao, marca, fabricante, imagem;
	private double valor;
	private TipoProdutoEnum tipo;
	private ArrayList<Fornecedor> fornecedor;
	
	// quando retornado do banco
	public Produto(long id, int quantidade,
			String nomeProduto, String descricao, String marca,
			String fabricante, String imagem, double valor, TipoProdutoEnum tipo){
		setId(id);
		setQuantidade(quantidade);
		setNomeProduto(nomeProduto);
		setDescricao(descricao);
		setMarca(marca);
		setFabricante(fabricante);
		setImagem(imagem);
		setValor(valor);
	}
	
	// quando criado para inserir no banco
	public Produto(int quantidade,
			String nomeProduto, String descricao, String marca,
			String fabricante, String imagem, double valor, TipoProdutoEnum tipo){
		setQuantidade(quantidade);
		setNomeProduto(nomeProduto);
		setDescricao(descricao);
		setMarca(marca);
		setFabricante(fabricante);
		setImagem(imagem);
		setValor(valor);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public String getNomeProduto() {
		return nomeProduto;
	}

	public void setNomeProduto(String nomeProduto) {
		this.nomeProduto = nomeProduto;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getFabricante() {
		return fabricante;
	}

	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}

	public String getImagem() {
		return imagem;
	}

	public void setImagem(String imagem) {
		this.imagem = imagem;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public TipoProdutoEnum getTipo() {
		return tipo;
	}

	public void setTipo(TipoProdutoEnum tipo) {
		this.tipo = tipo;
	}

	public ArrayList<Fornecedor> getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(ArrayList<Fornecedor> fornecedor) {
		this.fornecedor = fornecedor;
	}

}