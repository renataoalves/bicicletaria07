package br.novaroma.bicicletaria07.model;

public class ContatoFuncionario extends Contato {

	private String emailEmpresarial;
	
	// quando retornado do banco
	public ContatoFuncionario(long id, String email, String celular, String emailEmpresarial) {
		super(id, email, celular);
		setEmailEmpresarial(emailEmpresarial);
	}
	// quando retornado do banco
	public ContatoFuncionario(long id, String email, String celular, String telefoneFixo, String emailEmpresarial) {
		super(id, email, celular, telefoneFixo);
		setEmailEmpresarial(emailEmpresarial);
	}
	
	// quando criado para inserir no banco
	public ContatoFuncionario(String email, String celular, String emailEmpresarial) {
		super(email, celular);
		setEmailEmpresarial(emailEmpresarial);
	}
	
	// quando criado para inserir no banco
	public ContatoFuncionario(String email, String celular, String telefoneFixo, String emailEmpresarial) {
		super(email, celular, telefoneFixo);
		setEmailEmpresarial(emailEmpresarial);
	}

	public String getEmailEmpresarial() {
		return emailEmpresarial;
	}

	public void setEmailEmpresarial(String emailEmpresarial) {
		this.emailEmpresarial = emailEmpresarial;
	}
	
}
