package br.novaroma.bicicletaria07.model;

import java.time.LocalDate;

import br.novaroma.bicicletaria07.controller.EncriptaSenha;
import br.novaroma.bicicletaria07.enums.SexoEnum;
import br.novaroma.bicicletaria07.enums.TipoFuncionarioEnum;

public class Funcionario extends Pessoa {
	
	private String login, senha;
	private ContatoFuncionario contato;
	private TipoFuncionarioEnum tipoFuncionario;
	
	// quando retornado do banco
	public Funcionario(long id, String login, String senha, String nome, String cpf,
			SexoEnum sexo, LocalDate dataNascimento, ContatoFuncionario contato, Endereco endereco,
			TipoFuncionarioEnum tipoFuncionario){
		super(id, nome, cpf, sexo, dataNascimento, endereco);
		setLogin(login);
		setSenha(senha);
		setContato(contato);
		setTipoFuncionario(tipoFuncionario);
	}
	
	// quando criado para inserir no banco
	public Funcionario(String login, String senha, String nome, String cpf,
			SexoEnum sexo, LocalDate dataNascimento, ContatoFuncionario contato, Endereco endereco,
			TipoFuncionarioEnum tipoFuncionario){
		super(nome, cpf, sexo, dataNascimento, endereco);
		setLogin(login);
		setSenha(senha);
		setContato(contato);
		setTipoFuncionario(tipoFuncionario);
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = EncriptaSenha.encriptar(senha);
	}

	public TipoFuncionarioEnum getTipoFuncionario() {
		return tipoFuncionario;
	}

	public void setTipoFuncionario(TipoFuncionarioEnum tipoFuncionario) {
		this.tipoFuncionario = tipoFuncionario;
	}

	public ContatoFuncionario getContato() {
		return contato;
	}

	public void setContato(ContatoFuncionario contato) {
		this.contato = contato;
	}
	
}