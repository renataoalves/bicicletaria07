package br.novaroma.bicicletaria07.model;

import java.time.LocalDate;

import br.novaroma.bicicletaria07.enums.SexoEnum;

public class ClienteFisico extends Pessoa {
	
	private Contato contato;
	
	// quando retornado do banco
	public ClienteFisico (long id, String nome, String cpf,
			SexoEnum sexo, LocalDate dataNascimento, Endereco endereco){
		super(id, nome, cpf, sexo, dataNascimento, endereco);
		setContato(contato);
	}
	
	// quando criado para inserir no banco
	public ClienteFisico (String nome, String cpf,
			SexoEnum sexo, LocalDate dataNascimento,
			Contato contato, Endereco endereco){
		super(nome, cpf, sexo, dataNascimento, endereco);
		setContato(contato);
	}

	public Contato getContato() {
		return contato;
	}

	public void setContato(Contato contato) {
		this.contato = contato;
	}

}