package br.novaroma.bicicletaria07.componente;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.ParseException;

import javax.swing.border.LineBorder;

import br.novaroma.bicicletaria07.interfaces.ILimparCampos;
import br.novaroma.bicicletaria07.interfaces.IRemoverObrigatoriedade;

public class JPanelContato extends JPanelPattern implements KeyListener, ILimparCampos, IRemoverObrigatoriedade {

	private static final long serialVersionUID = 1L;
	
	private JLabelObrigatoria[] lbl;
	private JFieldPadrao txtEmail;
	private JFieldTelFixo txtTelFixo;
	private JFieldTelCelular txtTelCel;
	
	public JPanelContato() {
		super("Contato");
		try{
			add(new JLabelPattern("Tel Fixo:")).setBounds(40, 20, 83, 14);
			
			txtTelFixo = new JFieldTelFixo();
			txtTelFixo.addKeyListener(this);
			add(txtTelFixo).setBounds(90, 20, 90, 20);
			
			lbl = new JLabelObrigatoria[2];
			
			lbl[0] = new JLabelObrigatoria();
			add(lbl[0]).setBounds(10, 50, 16, 14);
			
			add(new JLabelPattern("Tel Celular:")).setBounds(23, 50, 100, 14);
			
			txtTelCel = new JFieldTelCelular();
			txtTelCel.addKeyListener(this);
			add(txtTelCel).setBounds(90, 50, 100, 20);
			
			lbl[1] = new JLabelObrigatoria();
			add(lbl[1]).setBounds(190, 20, 16, 14);
			
			add(new JLabelPattern("E-mail:")).setBounds(197, 20, 41, 14);
			
			txtEmail = new JFieldPadrao();
			txtEmail.addKeyListener(this);
			add(txtEmail).setBounds(240, 20, 435, 20);
			
		} catch (ParseException e) {}
	}
	
	@Override
	public void keyPressed(KeyEvent e) {}
	@Override
	public void keyReleased(KeyEvent e) {
		if(e.getSource() == txtTelFixo)
			if(txtTelFixo.getText().trim().length() < 13)
				txtTelFixo.setBorder(new LineBorder(Color.RED));
			else
				txtTelFixo.setBorder(new LineBorder(Color.LIGHT_GRAY));
		else if(e.getSource() == txtTelCel)
			if(txtTelCel.getText().trim().length() < 14)
				txtTelCel.setBorder(new LineBorder(Color.RED));
			else
				txtTelCel.setBorder(new LineBorder(Color.LIGHT_GRAY));
		else if(e.getSource() == txtEmail)
			if(txtEmail.getText().trim().length() < 4)
				txtEmail.setBorder(new LineBorder(Color.RED));
			else
				txtEmail.setBorder(new LineBorder(Color.LIGHT_GRAY));
	}
	@Override
	public void keyTyped(KeyEvent e) {}
	
	@Override
	public void limparCampos(){
		txtEmail.setText("");
		txtTelFixo.setText("");
		txtTelCel.setText("");
		repaint();
	}
	
	@Override
	public void removerObrigatoriedade(){
		for (JLabelObrigatoria l: lbl)
			remove(l);
		repaint();
	}

	/*public static void main(String[] args) {
		JFramePadrao a = new JFramePadrao();
		a.getContentPane().add(new JPanelContato()).setBounds(0, 0, 690, 85);
		a.setSize(700, 120);
		a.setLocationRelativeTo(null);
	}*/
}