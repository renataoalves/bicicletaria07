package br.novaroma.bicicletaria07.componente;

import java.awt.Rectangle;

import javax.swing.JList;
import javax.swing.JScrollPane;

import br.novaroma.bicicletaria07.interfaces.ILimparCampos;
import br.novaroma.bicicletaria07.interfaces.IRemoverObrigatoriedade;
import br.novaroma.bicicletaria07.model.Produto;

public class JPanelProdutoLista extends JPanelPattern implements ILimparCampos, IRemoverObrigatoriedade {
	
	private static final long serialVersionUID = 1L;
	
	private JLabelObrigatoria lbl;
	private JList<Produto> lista;
	private JScrollPane scroll;
	private JButtonPattern btn;
	
	public JPanelProdutoLista(){
		super("Produtos");
		
		lbl = new JLabelObrigatoria();
		add(lbl).setBounds(15, 15, 7, 14);
		
		add(new JLabelPattern("Produto (s):")).setBounds(25, 15, 70, 14);
		
		lista = new JList<Produto>();
		lista.scrollRectToVisible(new Rectangle());
		scroll = new JScrollPane(lista, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER); 
		add(scroll).setBounds(95, 15, 465, 100);
		
		btn = new JButtonPattern("Adicionar");
		add(btn).setBounds(570, 15, 100, 25);
	}

	@Override
	public void limparCampos() {
		lista.removeAll();
		repaint();
	}

	@Override
	public void removerObrigatoriedade(){
		remove(lbl);
		repaint();
	}

	public JButtonPattern getBtn() {
		return btn;
	}

	/*public static void main(String[] args) {
		JFramePadrao a = new JFramePadrao();
		a.getContentPane().add(new JPanelProdutoLista()).setBounds(0,0,680,190);;
		a.setVisible(true);
		a.setSize(695, 200);
		a.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		a.setLocationRelativeTo(null);
	}*/

}