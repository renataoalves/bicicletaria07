package br.novaroma.bicicletaria07.componente;

import java.awt.Font;

import javax.swing.JButton;

public class JButtonPattern extends JButton {
	
	private static final long serialVersionUID = 1L;

	public JButtonPattern(String string) {
		super(string);
		setFont(new Font("Verdana", Font.PLAIN, 11));
	}

}