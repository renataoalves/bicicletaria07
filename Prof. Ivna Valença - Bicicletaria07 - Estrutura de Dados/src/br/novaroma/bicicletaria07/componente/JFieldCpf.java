package br.novaroma.bicicletaria07.componente;

import java.text.ParseException;

import javax.swing.text.MaskFormatter;

public class JFieldCpf extends JFFieldPattern {

	private static final long serialVersionUID = 1L;

	public JFieldCpf() throws ParseException {
		super(getMascara());
	}
	
	public JFieldCpf(String cpf) throws ParseException {
		super(getMascara());
		setText(cpf);
	}
	
	private static MaskFormatter getMascara() throws ParseException{
		MaskFormatter masc = new MaskFormatter("###.###.###-##");
		masc.setPlaceholderCharacter(' ');
		return masc;
	}
	
}