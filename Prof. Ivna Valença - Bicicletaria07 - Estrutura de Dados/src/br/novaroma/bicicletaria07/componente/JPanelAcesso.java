package br.novaroma.bicicletaria07.componente;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import br.novaroma.bicicletaria07.interfaces.ILimparCampos;
import br.novaroma.bicicletaria07.interfaces.IRemoverObrigatoriedade;

public class JPanelAcesso extends JPanelPattern implements KeyListener, ILimparCampos, IRemoverObrigatoriedade {

	private static final long serialVersionUID = 1L;
	
	private JLabelObrigatoria[] lbl;
	private JTextField txtLogin;
	private JPasswordField txtSenha;
	
	public JPanelAcesso() {
		super("Acesso");
		
		lbl = new JLabelObrigatoria[2];
		
		lbl[0] = new JLabelObrigatoria();
		add(lbl[0]).setBounds(27, 20, 7, 14);
		
		add(new JLabelPattern("Login:")).setBounds(35, 20, 50, 14);
		
		txtLogin = new JTextField(5);
		txtLogin.setDocument(new DocumentUpperCase());
		txtLogin.addKeyListener(this);
		add(txtLogin).setBounds(73, 20, 190, 20);
		
		lbl[1] = new JLabelObrigatoria();
		add(lbl[1]).setBounds(23, 45, 7, 14);
		
		add(new JLabelPattern("Senha:")).setBounds(30, 45, 50, 14);
		
		txtSenha = new JPasswordField();
		txtSenha.addKeyListener(this);
		add(txtSenha).setBounds(73, 45, 190, 20);
	}
	
	@Override
	public void keyPressed(KeyEvent e) {}
	@Override
	public void keyReleased(KeyEvent e) {
		if(e.getSource() == txtLogin)
			if(txtLogin.getText().trim().length() < 6)
				txtLogin.setBorder(new LineBorder(Color.RED));
			else
				txtLogin.setBorder(new LineBorder(Color.LIGHT_GRAY));
		else if(e.getSource() == txtSenha)
			if(new String(txtSenha.getPassword()).trim().length() < 6)
				txtSenha.setBorder(new LineBorder(Color.RED));
			else
				txtSenha.setBorder(new LineBorder(Color.LIGHT_GRAY));
	}
	@Override
	public void keyTyped(KeyEvent e) {}
	
	@Override
	public void limparCampos(){
		txtLogin.setText("");
		txtSenha.setText("");
	}
	
	@Override
	public void removerObrigatoriedade(){
		for (JLabelObrigatoria l: lbl)
			remove(l);
		repaint();
	}
	
	/*public static void main(String[] args) {
		JFramePadrao a = new JFramePadrao();
		a.add(new JPanelAcesso()).setBounds(0,0,290,75);
		a.setSize(380, 120);
		a.setLocationRelativeTo(null);
	}*/
	
}