package br.novaroma.bicicletaria07.componente;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JFormattedTextField;
import javax.swing.text.MaskFormatter;

public class JFFieldPattern extends JFormattedTextField {

	private static final long serialVersionUID = 1L;

	public JFFieldPattern() {}
	
	public JFFieldPattern(String string){
		super(string);
		setFont();
	}
	
	public JFFieldPattern(String string, Color color){
		super(string);
		setForeground(color);
		setFont();
	}
	
	public JFFieldPattern(MaskFormatter mask){
		super(mask);
		setFont();
	}
	
	private void setFont(){
		setFont(new Font("Verdana", Font.PLAIN, 11));
	}
	
}