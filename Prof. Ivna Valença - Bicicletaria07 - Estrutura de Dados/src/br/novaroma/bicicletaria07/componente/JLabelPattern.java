package br.novaroma.bicicletaria07.componente;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import java.awt.Color;
import java.awt.Font;

public class JLabelPattern extends JLabel {

	private static final long serialVersionUID = 1L;

	public JLabelPattern(String string){
		super(string);
		setFont(11);
	}
	
	public JLabelPattern(String string, int tam){
		super(string);
		setFont(tam);
	}
	
	public JLabelPattern(ImageIcon img){
		super(img);
	}
	
	public JLabelPattern(String string, Color color){
		super(string);
		setForeground(color);
		setFont(11);
	}
	
	private void setFont(int tam){
		setFont(new Font("Verdana", Font.PLAIN, tam));
	}
	
}