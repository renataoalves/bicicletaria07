package br.novaroma.bicicletaria07.componente;

public class JIFrameProduto extends JIFramePattern {

	private static final long serialVersionUID = 1L;
	
	private JPanelProduto panel;
	
	public JIFrameProduto(){
		super("Adicionar Produto");
		
		panel = new JPanelProduto();
		setContentPane(panel);
		
		setSize(495, 275);
	}
	
}