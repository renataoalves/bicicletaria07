package br.novaroma.bicicletaria07.componente;

import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Properties;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.KeyStroke;
import javax.swing.UIManager;

import com.jtattoo.plaf.texture.TextureLookAndFeel;

public class JFramePattern extends JFrame {
	
	private static final long serialVersionUID = 1L;

	public JFramePattern(){
		criar();
	}
	
	public JFramePattern(String titulo){
		super(titulo);
		criar();
	}
	
	public void criar(){
    	try {
    		Properties props = new Properties();
			props.put("logoString", "");
			TextureLookAndFeel.setCurrentTheme(props);
			UIManager.setLookAndFeel("com.jtattoo.plaf.texture.TextureLookAndFeel");
			  
			setIconImage((new ImageIcon(getClass().getResource("/br/novaroma/bicicletaria07/imagem/icone.png"))).getImage());
			getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "EscPressed");
	        addWindowListener(new WindowAdapter() {
	            public void windowClosing(WindowEvent evt){
	            	dispose();
	            }
	        });
	        
			setLayout(null);
			setVisible(true);
			setResizable(false);
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
    	} catch (Exception ex) {
            ex.printStackTrace();
        }
	}
	
	//public static void main(String[] args) { new JFramePadrao();	}

}