package br.novaroma.bicicletaria07.componente;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JTextField;

public class JFieldPadrao extends JTextField {

	private static final long serialVersionUID = 1L;

	public JFieldPadrao(){
		super("");
		setFont();
	}
	
	public JFieldPadrao(String string){
		super(string);
		setFont();
	}
	
	public JFieldPadrao(int inteiro){
		super(inteiro);
		setFont();
	}
	
	public JFieldPadrao(String string, Color color){
		super(string);
		setForeground(color);
		setFont();
	}
	
	private void setFont(){
		setFont(new Font("Verdana", Font.PLAIN, 11));
	}
	
	@Override
	public void setText(String string) {
		super.setText(string);
	}
	
}