package br.novaroma.bicicletaria07.componente;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.border.LineBorder;

import br.novaroma.bicicletaria07.interfaces.ILimparCampos;
import br.novaroma.bicicletaria07.interfaces.IRemoverObrigatoriedade;

public class JPanelContatoFuncionario extends JPanelContato implements KeyListener, ILimparCampos, IRemoverObrigatoriedade {

	private static final long serialVersionUID = 1L;
	
	private JLabelObrigatoria lbl;
	private JFieldPadrao txtEmailE;
	
	public JPanelContatoFuncionario(){
		lbl = new JLabelObrigatoria();
		add(lbl).setBounds(192, 50, 16, 14);
		
		add(new JLabelPattern("E-mail empresarial:")).setBounds(200, 50, 120, 14);
		
		txtEmailE = new JFieldPadrao();
		txtEmailE.setDocument(new DocumentUpperCase());
		txtEmailE.addKeyListener(this);
		add(txtEmailE).setBounds(315, 50, 288, 20);
		
		add(new JLabelPattern("@gmail.com")).setBounds(605, 50, 74, 14);
	}
	
	@Override
	public void keyPressed(KeyEvent e) {}
	@Override
	public void keyReleased(KeyEvent e) {
		super.keyReleased(e);
		if(e.getSource() == txtEmailE)
			if(txtEmailE.getText().trim().length() < 5)
				txtEmailE.setBorder(new LineBorder(Color.RED));
			else
				txtEmailE.setBorder(new LineBorder(Color.LIGHT_GRAY));
	}
	@Override
	public void keyTyped(KeyEvent e) {}
	
	@Override
	public void limparCampos(){
		txtEmailE.setText("");
		super.limparCampos();
	}

	@Override
	public void removerObrigatoriedade(){
		remove(lbl);
		super.removerObrigatoriedade();
	}
	
//	public static void main(String[] args) {
//		JFramePadrao a = new JFramePadrao();
//		a.getContentPane().add(new JPanelContatoFuncionario()).setBounds(0, 0, 690, 85);
//		a.setSize(700, 120);
//		a.setLocationRelativeTo(null);
//	}
	
}