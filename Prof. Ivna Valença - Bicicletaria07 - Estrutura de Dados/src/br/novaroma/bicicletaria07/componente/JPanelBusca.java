package br.novaroma.bicicletaria07.componente;

import br.novaroma.bicicletaria07.interfaces.ILimparCampos;
import br.novaroma.bicicletaria07.interfaces.IRemoverObrigatoriedade;
import br.novaroma.bicicletaria07.model.ClienteFisico;
import br.novaroma.bicicletaria07.model.ClienteJuridico;
import br.novaroma.bicicletaria07.model.Fornecedor;
import br.novaroma.bicicletaria07.model.Funcionario;
import br.novaroma.bicicletaria07.model.Produto;
import br.novaroma.bicicletaria07.model.Servico;

public class JPanelBusca extends JPanelPattern implements ILimparCampos, IRemoverObrigatoriedade {

	private static final long serialVersionUID = 1L;
	
	private JPanelCadastro pnPessoaEmpresa;
	private JPanelProduto pnProduto;
	private JPanelServico pnServico;
	
	public JPanelBusca(Class<?> tipo){
		if(tipo == ClienteFisico.class || tipo == Funcionario.class || tipo == ClienteJuridico.class || tipo == Fornecedor.class)
			pnPessoaEmpresa = new JPanelCadastro(tipo);
		else if (tipo == Produto.class)
			pnProduto = new JPanelProduto();
		else if (tipo == Servico.class)
			pnServico = new JPanelServico();
		
		if(tipo == Funcionario.class)
			add(pnPessoaEmpresa).setBounds(5, 15, 690, 395);
		else if(tipo == ClienteFisico.class || tipo == ClienteJuridico.class || tipo == Fornecedor.class)
			add(pnPessoaEmpresa).setBounds(5, 15, 690, 310);
		else if(tipo == Produto.class)
			add(pnProduto).setBounds(5, 15, 490, 210);
		else if(tipo == Servico.class)
			add(pnServico).setBounds(5, 15, 480, 180);
	}

	@Override
	public void limparCampos() {
		if(pnPessoaEmpresa != null)
			pnPessoaEmpresa.limparCampos();
		if(pnProduto != null)
			pnProduto.limparCampos();
		if(pnServico != null)
			pnServico.limparCampos();
		repaint();
	}
	
	@Override
	public void removerObrigatoriedade() {
		if(pnPessoaEmpresa != null)
			pnPessoaEmpresa.removerObrigatoriedade();
		if(pnProduto != null)
			pnProduto.removerObrigatoriedade();
		if(pnServico != null)
			pnServico.removerObrigatoriedade();
		repaint();
	}

	/*public static void main(String[] args) {
		JFrame a = new JFrame();
		a.add(new JPanelBusca(Servico.class));
		a.setVisible(true);
		a.setSize(800, 500);
		a.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		a.setLocationRelativeTo(null);
	}*/

}