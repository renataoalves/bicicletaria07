package br.novaroma.bicicletaria07.componente;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import br.novaroma.bicicletaria07.model.Servico;

public abstract class JIFrameServico extends JIFramePattern {

	private static final long serialVersionUID = 1L;

	private DefaultListModel<Servico> listaLIM, listaMAN, listaMON, listaPIN;
	private JList<Servico> jlistLimpeza, jlistManutencao, jlistMontagem, jlistlistaPintura;
	private JScrollPane scrLi, scrMa, scrMo, scrPi;
	private JButtonPattern btn;
	
	public JIFrameServico(String modulo) {
		if(modulo.equals("Atendimento"))
			super.setTitle("Fila de Atendimento de Servi�os");
		else
			super.setTitle("Sevi�os em Andamento");
		
		add(new JLabelPattern("Servi�o de Limpeza:")).setBounds(10, 5, 150, 20);
		add(new JLabelPattern("Servi�o de Montagem:")).setBounds(330, 5, 150, 20);
		add(new JLabelPattern("Servi�o de Manuten��o:")).setBounds(10, 285, 150, 20);
		add(new JLabelPattern("Servi�o de Pintura:")).setBounds(330, 285, 150, 20);
		
		listaLIM = new DefaultListModel<Servico>();
		jlistLimpeza = new JList<Servico>(listaLIM);
		jlistLimpeza.setBounds(35, 32, 495, 128);
		scrLi = new JScrollPane(jlistLimpeza, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER); 
		add(scrLi).setBounds(10, 30, 300, 250);
		
		listaMON = new DefaultListModel<Servico>();
		jlistMontagem = new JList<Servico>(listaMON);
		scrMo = new JScrollPane(jlistMontagem, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		add(scrMo).setBounds(330, 30, 300, 250);
		
		listaMAN = new DefaultListModel<Servico>();
		jlistManutencao = new JList<Servico>(listaMAN);
		scrMa = new JScrollPane(jlistManutencao, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		add(scrMa).setBounds(10, 310, 300, 250);
		
		listaPIN = new DefaultListModel<Servico>();
		jlistlistaPintura = new JList<Servico>(listaPIN);
		scrPi = new JScrollPane(jlistlistaPintura, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		add(scrPi).setBounds(330, 310, 300, 250);
		
		if(modulo.equals("Atendimento"))
			btn = new JButtonPattern("Adicionar");
		else
			btn = new JButtonPattern("Finalizar");
		add(btn).setBounds(265, 565, 100, 25);
		
		setSize(775, 645);
	}

//	public static void main(String[] args) {
//		JFrame a = new JFrame();
//		a.add(new JIFrameServico("Atendimento"));
//		a.setSize(775, 645);
//		a.setVisible(true);
//		a.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		a.setLocationRelativeTo(null);
//	}

}