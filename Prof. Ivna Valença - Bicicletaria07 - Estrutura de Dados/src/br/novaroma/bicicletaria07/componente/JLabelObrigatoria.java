package br.novaroma.bicicletaria07.componente;

import java.awt.Color;

public class JLabelObrigatoria extends JLabelPattern {
	
	private static final long serialVersionUID = 1L;

	public JLabelObrigatoria(){
		super("*");
		setForeground();
	}
	
	public JLabelObrigatoria(String texto){
		super(texto);
		setForeground();
	}
	
	private void setForeground(){
		setForeground(Color.RED);
	}

}