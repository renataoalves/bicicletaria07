package br.novaroma.bicicletaria07.componente;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.ParseException;

import javax.swing.border.LineBorder;

import br.novaroma.bicicletaria07.enums.TipoServicoEnum;
import br.novaroma.bicicletaria07.interfaces.ILimparCampos;
import br.novaroma.bicicletaria07.interfaces.IRemoverObrigatoriedade;

public class JPanelServico extends JPanelPattern implements ActionListener, KeyListener, ILimparCampos, IRemoverObrigatoriedade {

	private static final long serialVersionUID = 1L;
	
	private JLabelObrigatoria[] lbl;
	private JFieldCpf txtCpf;
	private JFieldPadrao txtNome, txtCodFunc;
	private JAreaPattern txtDescricao;
	private JComboBoxTipos combo;
	
	public JPanelServico() {
		super("Servi�o");
		inicialize(-1, null, null, null, null);
	}
	
	public JPanelServico(int codigo){
		super("Servi�o");
		inicialize(codigo, null, null, null, null);
	}
	
	public JPanelServico(String cpf, int codigo, String nome, String descricao, String tipo){
		super("Servi�o");
		inicialize(codigo, cpf, nome, descricao, tipo);
	}
	
	private void inicialize(int codigo, String cpf, String nome, String descricao, String tipo){
		try{
			lbl = new JLabelObrigatoria[3];
			
			lbl[0] = new JLabelObrigatoria();
			add(lbl[0]).setBounds(14, 25, 7, 14);
			
			add(new JLabelPattern("Nome: ")).setBounds(25, 25, 65, 14);

			txtNome = new JFieldPadrao();
			if(nome != null)
				txtNome = new JFieldPadrao(nome);
			txtNome.setDocument(new DocumentUpperCase());
			txtNome.addKeyListener(this);
			add(txtNome).setBounds(67, 25, 400, 20);
			
			lbl[1] = new JLabelObrigatoria();
			add(lbl[1]).setBounds(27, 50, 7, 14);
			
			add(new JLabelPattern("CPF: ")).setBounds(37, 50, 45, 14);
			
			txtCpf = new JFieldCpf();
			txtCpf.addKeyListener(this);
			if(cpf != null)
				txtCpf = new JFieldCpf(cpf);
			add(txtCpf).setBounds(67, 50, 97, 20);
			
			if(codigo != -1){
				add(new JLabelPattern("COD.FUNC.: ")).setBounds(345, 50, 80, 14);
				
				txtCodFunc = new JFieldPadrao(codigo);
				txtCodFunc.setEditable(false);
				add(txtCodFunc).setBounds(422, 50, 45, 20);
			}
			
			lbl[2] = new JLabelObrigatoria();
			add(lbl[2]).setBounds(14, 75, 7, 14);
			
			add(new JLabelPattern("Tipo de Servi�o: ")).setBounds(25, 75, 100, 14);
			
			combo = new JComboBoxTipos(TipoServicoEnum.class);
			combo.addActionListener(this);
			if(tipo != null){
				combo = new JComboBoxTipos(Enum.valueOf(TipoServicoEnum.class, tipo).getClass());
				combo.setSelectedItem(identificarItem(combo.getSelectedObjects()));
			}
			add(combo).setBounds(125, 75, 130, 20);
			
			add(new JLabelPattern("Descri��o: ")).setBounds(25, 100, 65, 14);
			
			txtDescricao = new JAreaPattern();
			if(descricao != null)
				txtDescricao = new JAreaPattern(descricao);
			add(txtDescricao).setBounds(88, 100, 380, 70);

		} catch (ParseException e) {}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == combo)
			if(combo.getSelectedIndex() == 0)
				combo.setBorder(new LineBorder(Color.RED));
			else
				combo.setBorder(new LineBorder(Color.LIGHT_GRAY));
	}
	
	@Override
	public void keyPressed(KeyEvent e) {}
	@Override
	public void keyReleased(KeyEvent e) {
		if(e.getSource() == txtNome)
			if(txtNome.getText().trim().length() == 0)
				txtNome.setBorder(new LineBorder(Color.RED));
			else
				txtNome.setBorder(new LineBorder(Color.LIGHT_GRAY));
		else if(e.getSource() == txtCpf)
			if(txtCpf.getText().trim().length() < 14)
				txtCpf.setBorder(new LineBorder(Color.RED));
			else
				txtCpf.setBorder(new LineBorder(Color.LIGHT_GRAY));
	}
	@Override
	public void keyTyped(KeyEvent e) {}
	
	@Override
	public void limparCampos(){
		txtCpf.setText("");
		txtNome.setText("");
		txtCodFunc.setText("");
		txtDescricao.setText("");
		combo.setSelectedIndex(0);
		repaint();
	}
	
	@Override
	public void removerObrigatoriedade(){
		for (JLabelObrigatoria l: lbl)
			remove(l);
		repaint();
	}

	private int identificarItem(Object[] obj){
		if(obj.equals(TipoServicoEnum.LIMPEZA))
			return 0;
		else if(obj.equals(TipoServicoEnum.MANUTENCAO))
			return 1;
		else if(obj.equals(TipoServicoEnum.MONTAGEM))
			return 2;
		else
			return 3;
	}
	
//	public static void main(String[] args) {
//		JFramePadrao a = new JFramePadrao();
//		a.getContentPane().add(new JPanelServico(0)).setBounds(0, 0, 480, 205);
//		a.setSize(490, 250);
//		a.setLocationRelativeTo(null);
//	}
	
}