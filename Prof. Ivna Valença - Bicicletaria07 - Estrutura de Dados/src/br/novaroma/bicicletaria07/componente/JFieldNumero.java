package br.novaroma.bicicletaria07.componente;

import java.text.ParseException;

import javax.swing.text.MaskFormatter;

public class JFieldNumero extends JFFieldPattern {

	private static final long serialVersionUID = 1L;

	public JFieldNumero(int tam) throws ParseException {
		super(getMascara(tam));
	}
	
	public static MaskFormatter getMascara(int tam) throws ParseException{
		MaskFormatter masc = new MaskFormatter(stringMascara(tam));
		masc.setPlaceholderCharacter(' ');
		masc.setValidCharacters("0123456789");
		return masc;
	}
	
	private static String stringMascara(int tam){
		String t = "";
		for(int x=1; x<=tam; x++) t+="#";
		return t;
	}
	
}