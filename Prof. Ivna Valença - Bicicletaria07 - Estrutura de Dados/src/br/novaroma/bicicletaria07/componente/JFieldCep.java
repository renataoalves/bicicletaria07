package br.novaroma.bicicletaria07.componente;

import java.text.ParseException;

import javax.swing.text.MaskFormatter;

public class JFieldCep extends JFFieldPattern {

	private static final long serialVersionUID = 1L;

	public JFieldCep() throws ParseException {
		super(getMascara());
	}
	
	public JFieldCep(String cep) throws ParseException {
		super(getMascara());
		setText(cep);
	}
	
	private static MaskFormatter getMascara() throws ParseException{
		MaskFormatter masc = new MaskFormatter("#####-###");
		masc.setPlaceholderCharacter(' ');
		return masc;
	}
	
}