package br.novaroma.bicicletaria07.componente;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.border.LineBorder;

import br.novaroma.bicicletaria07.interfaces.ILimparCampos;
import br.novaroma.bicicletaria07.interfaces.IRemoverObrigatoriedade;

public class JPanelDefinicaoPapel extends JPanelPattern implements ActionListener, ILimparCampos, IRemoverObrigatoriedade {

	private static final long serialVersionUID = 1L;

	private JLabelObrigatoria lbl;
	private JComboBoxTipos combo;
	
	public JPanelDefinicaoPapel(String string, Class<?> classe){
		super("Defini��o de Papel");
		
		lbl = new JLabelObrigatoria();
		add(lbl).setBounds(20, 20, 7, 14);
		
		add(new JLabelPattern(string)).setBounds(28, 20, 125, 15);
		
		combo = new JComboBoxTipos(classe);
		combo.addActionListener(this);
		add(combo).setBounds(150, 20, 120, 20);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == combo)
			if(combo.getSelectedIndex() == 0)
				combo.setBorder(new LineBorder(Color.RED));
			else
				combo.setBorder(new LineBorder(Color.LIGHT_GRAY));
	}
	
	@Override
	public void limparCampos(){
		combo.setSelectedIndex(0);
		repaint();
	}
	
	@Override
	public void removerObrigatoriedade(){
		remove(lbl);
		repaint();
	}

	/*public static void main(String[] args) {
		JFramePadrao a = new JFramePadrao();
		a.getContentPane().add(new JPanelDefinicaoPapel("asd", TipoFuncionarioEnum.class)).setBounds(0, 0, 690, 85);
		a.setSize(700, 120);
		a.setLocationRelativeTo(null);
	}*/

}