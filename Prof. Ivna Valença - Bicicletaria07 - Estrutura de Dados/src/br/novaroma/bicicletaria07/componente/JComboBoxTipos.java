package br.novaroma.bicicletaria07.componente;

import java.awt.Font;

import javax.swing.JComboBox;

import br.novaroma.bicicletaria07.enums.TipoEnderecoEnum;
import br.novaroma.bicicletaria07.enums.TipoFuncionarioEnum;
import br.novaroma.bicicletaria07.enums.TipoProdutoEnum;
import br.novaroma.bicicletaria07.enums.TipoServicoEnum;

public class JComboBoxTipos extends JComboBox<String>{

	private static final long serialVersionUID = 1L;

	public JComboBoxTipos(Class<?> tipoClasse) {
		addItem("-- SELECIONE --");
		if(tipoClasse == TipoFuncionarioEnum.class){
			addItem(String.valueOf(TipoFuncionarioEnum.ADM));
			addItem(String.valueOf(TipoFuncionarioEnum.MECANICO));
			addItem(String.valueOf(TipoFuncionarioEnum.PINTOR));
			addItem(String.valueOf(TipoFuncionarioEnum.VENDEDOR));
		} else if(tipoClasse == TipoProdutoEnum.class){
			addItem(String.valueOf(TipoProdutoEnum.ACESSORIO));
			addItem(String.valueOf(TipoProdutoEnum.BICICLETA));
			addItem(String.valueOf(TipoProdutoEnum.PECA));
		} else if(tipoClasse == TipoServicoEnum.class){
			addItem(String.valueOf(TipoServicoEnum.LIMPEZA));
			addItem(String.valueOf(TipoServicoEnum.MANUTENCAO));
			addItem(String.valueOf(TipoServicoEnum.MONTAGEM));
			addItem(String.valueOf(TipoServicoEnum.PINTURA));
		} else if(tipoClasse == TipoEnderecoEnum.class){
			addItem(String.valueOf(TipoEnderecoEnum.ANEXO));
			addItem(String.valueOf(TipoEnderecoEnum.APARTAMENTO));
			addItem(String.valueOf(TipoEnderecoEnum.CASA));
			addItem(String.valueOf(TipoEnderecoEnum.COMERCIAL));
			addItem(String.valueOf(TipoEnderecoEnum.SOBRADO));
			addItem(String.valueOf(TipoEnderecoEnum.OUTRO));
		}
		setFont(new Font("Verdana", Font.PLAIN, 11));
	}
	
}