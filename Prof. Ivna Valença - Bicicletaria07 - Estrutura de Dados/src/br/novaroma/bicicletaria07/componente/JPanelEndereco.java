package br.novaroma.bicicletaria07.componente;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.ParseException;

import javax.swing.JComboBox;
import javax.swing.border.LineBorder;

import br.novaroma.bicicletaria07.enums.TipoEnderecoEnum;
import br.novaroma.bicicletaria07.interfaces.ILimparCampos;
import br.novaroma.bicicletaria07.interfaces.IRemoverObrigatoriedade;

public class JPanelEndereco extends JPanelPattern implements ActionListener, KeyListener, ILimparCampos, IRemoverObrigatoriedade {

	private static final long serialVersionUID = 1L;

	private JLabelObrigatoria[] lbl;
	private JFieldPadrao txtRua, txtBairro;
	private JFieldNumero txtN;
	private JFieldCep txtCep;
	private JAreaPattern txtComplemento;
	private JComboBox<String> comboEstado, comboCidade;
	private JComboBoxTipos comboTipoEndereco;
	
	public JPanelEndereco() {
		super("Endere�o");
		try{
			lbl = new JLabelObrigatoria[7];
			
			lbl[0] = new JLabelObrigatoria();
			add(lbl[0]).setBounds(12, 24, 7, 14);
			
			add(new JLabelPattern("Rua:")).setBounds(20, 26, 33, 14);
			
			txtRua = new JFieldPadrao();
			txtRua.setDocument(new DocumentUpperCase());
			txtRua.addKeyListener(this);
			add(txtRua).setBounds(50, 24, 444, 20);
			
			lbl[1] = new JLabelObrigatoria();
			add(lbl[1]).setBounds(499, 25, 7, 14);
			
			add(new JLabelPattern("Estado:")).setBounds(507, 25, 49, 14);
			
			comboEstado = new JComboBox<String>();
			comboEstado.addItem("--SELECIONE--");
			comboEstado.addItem("AC");
			comboEstado.addItem("AL");
			comboEstado.addItem("AM");
			comboEstado.addItem("AP");
			comboEstado.addItem("BA");
			comboEstado.addItem("CE");
			comboEstado.addItem("DF");
			comboEstado.addItem("ES");
			comboEstado.addItem("GO");
			comboEstado.addItem("MA");
			comboEstado.addItem("MG");
			comboEstado.addItem("MS");
			comboEstado.addItem("MT");
			comboEstado.addItem("PA");
			comboEstado.addItem("PB");
			comboEstado.addItem("PE");
			comboEstado.addItem("PI");
			comboEstado.addItem("PR");
			comboEstado.addItem("RJ");
			comboEstado.addItem("RN");
			comboEstado.addItem("RO");
			comboEstado.addItem("RR");
			comboEstado.addItem("RS");
			comboEstado.addItem("SC");
			comboEstado.addItem("SE");
			comboEstado.addItem("SP");
			comboEstado.addItem("TO");
			comboEstado.addActionListener(this);
			add(comboEstado).setBounds(554, 23, 118, 20);
			
			lbl[2] = new JLabelObrigatoria();
			add(lbl[2]).setBounds(19, 51, 7, 14);
			
			add(new JLabelPattern("N�:")).setBounds(28, 52, 19, 14);
			
			txtN = new JFieldNumero(5);
			txtN.addKeyListener(this);
			add(txtN).setBounds(50, 51, 44, 20);
			
			lbl[3] = new JLabelObrigatoria();
			add(lbl[3]).setBounds(97, 49, 7, 14);
			
			add(new JLabelPattern("Bairro:")).setBounds(106, 51, 41, 14);
			
			txtBairro = new JFieldPadrao();
			txtBairro.setDocument(new DocumentUpperCase());
			txtBairro.addKeyListener(this);
			add(txtBairro).setBounds(148, 51, 156, 20);
			
			lbl[4] = new JLabelObrigatoria();
			add(lbl[4]).setBounds(309, 50, 7, 14);
			
			add(new JLabelPattern("Cidade:")).setBounds(316, 51, 49, 14);
			
			comboCidade = new JComboBox<String>();
			comboCidade.addItem("-------------SELECIONE ESTADO-------------");
			comboCidade.addActionListener(this);
			add(comboCidade).setBounds(363, 50, 309, 20);
			
			lbl[5] = new JLabelObrigatoria();
			add(lbl[5]).setBounds(12, 79, 7, 14);
			
			add(new JLabelPattern("CEP:")).setBounds(19, 79, 33, 14);
			
			txtCep = new JFieldCep();
			txtCep.addKeyListener(this);
			add(txtCep).setBounds(50, 78, 68, 20);
			
			add(new JLabelPattern("Complemento:")).setBounds(188, 76, 91, 14);
			
			txtComplemento = new JAreaPattern();
			add(txtComplemento).setBounds(277, 77, 395, 48);
			
			lbl[6] = new JLabelObrigatoria();
			add(lbl[6]).setBounds(10, 106, 7, 14);
			
			add(new JLabelPattern("Tipo:")).setBounds(18, 106, 33, 14);
			
			comboTipoEndereco = new JComboBoxTipos(TipoEnderecoEnum.class);
			comboTipoEndereco.addActionListener(this);
			add(comboTipoEndereco).setBounds(50, 104, 220, 20);
			
		} catch (ParseException e) {}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == comboEstado)
			if(comboEstado.getSelectedIndex() == 0)
				comboEstado.setBorder(new LineBorder(Color.RED));
			else
				comboEstado.setBorder(new LineBorder(Color.LIGHT_GRAY));
		else if(e.getSource() == comboCidade)
			if(comboCidade.getSelectedIndex() == 0)
				comboCidade.setBorder(new LineBorder(Color.RED));
			else
				comboCidade.setBorder(new LineBorder(Color.LIGHT_GRAY));
		else if(e.getSource() == comboTipoEndereco)
			if(comboTipoEndereco.getSelectedIndex() == 0)
				comboTipoEndereco.setBorder(new LineBorder(Color.RED));
			else
				comboTipoEndereco.setBorder(new LineBorder(Color.LIGHT_GRAY));
	}
	
	@Override
	public void keyPressed(KeyEvent e) {}
	@Override
	public void keyReleased(KeyEvent e) {
		if(e.getSource() == txtRua)
			if(txtRua.getText().trim().length() == 0)
				txtRua.setBorder(new LineBorder(Color.RED));
			else
				txtRua.setBorder(new LineBorder(Color.LIGHT_GRAY));
		else if(e.getSource() == txtBairro)
			if(txtBairro.getText().trim().length() == 0)
				txtBairro.setBorder(new LineBorder(Color.RED));
			else
				txtBairro.setBorder(new LineBorder(Color.LIGHT_GRAY));
		else if(e.getSource() == txtN)
			if(txtN.getText().trim().length() == 0)
				txtN.setBorder(new LineBorder(Color.RED));
			else
				txtN.setBorder(new LineBorder(Color.LIGHT_GRAY));
		else if(e.getSource() == txtCep)
			if(txtCep.getText().trim().length() < 9)
				txtCep.setBorder(new LineBorder(Color.RED));
			else
				txtCep.setBorder(new LineBorder(Color.LIGHT_GRAY));
	}
	@Override
	public void keyTyped(KeyEvent e) {}
	
	@Override
	public void limparCampos(){
		txtRua.setText("");
		comboCidade.setSelectedIndex(0);
		txtN.setText("");
		txtBairro.setText("");
		comboEstado.setSelectedIndex(0);
		txtCep.setText("");
		txtComplemento.setText("");
		comboTipoEndereco.setSelectedIndex(0);
		repaint();
	}
	
	@Override
	public void removerObrigatoriedade(){
		for (JLabelObrigatoria l: lbl)
			remove(l);
		repaint();
	}

	/*public static void main(String[] args) {
		JFramePadrao a = new JFramePadrao();
		a.getContentPane().add(new JPanelEndereco()).setBounds(0, 0, 690, 150);
		a.setSize(700, 200);
		a.setLocationRelativeTo(null);
	}*/
	
}