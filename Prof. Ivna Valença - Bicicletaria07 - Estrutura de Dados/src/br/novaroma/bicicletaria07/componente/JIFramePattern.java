package br.novaroma.bicicletaria07.componente;

import java.awt.event.ActionEvent;  
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.KeyStroke;
import javax.swing.UIManager;

import br.novaroma.bicicletaria07.controller.TratarDados;


public class JIFramePattern extends JInternalFrame {

	private static final long serialVersionUID = 1L;

	public JIFramePattern(){
		super("", false, true, false, false);
		criar();
	}
	
	public JIFramePattern(String titulo){
		super(titulo, false, true, false, false);
		criar();
	}
	
	private void criar(){
		try {
			String tema = "texture.Texture";
			UIManager.setLookAndFeel("com.jtattoo.plaf."+tema+"LookAndFeel");
			
			setFrameIcon(TratarDados.redimensionar(20, 15, new ImageIcon(getClass().getResource("/br/novaroma/bicicletaria07/imagem/icone.png"))));
	        
			getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, false), "ESCAPE");  
			getRootPane().getActionMap().put("ESCAPE", new AbstractAction() {
				private static final long serialVersionUID = 1L;
				public void actionPerformed(ActionEvent e) { 
					dispose();
				}  
			});

			setLayout(null);
			setVisible(true);
			setSelected(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}