package br.novaroma.bicicletaria07.componente;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.ParseException;

import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;
import javax.swing.border.LineBorder;

import br.novaroma.bicicletaria07.interfaces.ILimparCampos;
import br.novaroma.bicicletaria07.interfaces.IRemoverObrigatoriedade;

public class JPanelPessoa  extends JPanelPattern implements ActionListener, KeyListener, ILimparCampos, IRemoverObrigatoriedade {
	
	private static final long serialVersionUID = 1L;
	
	private JLabelObrigatoria[] lbl;
	
	private ButtonGroup grpSexo;
	private JRadioButton btnFeminino, btnMasculino;
	
	private JFieldPadrao txtNome;
	private JFieldData txtData;
	private JFieldCpf txtCpf;

	public JPanelPessoa(){
		super("Informações Pessoais");
		try{
			lbl = new JLabelObrigatoria[4];
					
			lbl[0] = new JLabelObrigatoria();
			add(lbl[0]).setBounds(14, 25, 7, 14);
			
			add(new JLabelPattern("Nome Completo:")).setBounds(22, 24, 97, 14);
			
			txtNome = new JFieldPadrao();
			txtNome.setDocument(new DocumentUpperCase());
			txtNome.addKeyListener(this);
			add(txtNome).setBounds(122, 22, 552, 20);
			
			lbl[1] = new JLabelObrigatoria();
			add(lbl[1]).setBounds(15, 49, 7, 14);
			
			add(new JLabelPattern("Data de Nascimento:")).setBounds(23, 49, 124, 14);
			
			txtData = new JFieldData();
			txtData.addKeyListener(this);
			add(txtData).setBounds(146, 47, 72, 20);
			
			lbl[2] = new JLabelObrigatoria();
			add(lbl[2]).setBounds(285, 48, 7, 14);
			
			add(new JLabelPattern("Sexo:")).setBounds(293, 48, 34, 14);
			
			btnFeminino = new JRadioButton("Feminino");
			btnFeminino.addActionListener(this);
			btnMasculino = new JRadioButton("Masculino");
			btnMasculino.addActionListener(this);
			
			grpSexo = new ButtonGroup();
			grpSexo.add(btnFeminino);
			grpSexo.add(btnMasculino);
			
			add(btnFeminino).setBounds(330, 45, 83, 23);
			add(btnMasculino).setBounds(413, 45, 95, 23);
			
			lbl[3] = new JLabelObrigatoria();
			add(lbl[3]).setBounds(542, 49, 7, 14);
			
			add(new JLabelPattern("CPF:")).setBounds(550, 50, 41, 14);
			
			txtCpf = new JFieldCpf();
			txtCpf.addKeyListener(this);
			add(txtCpf).setBounds(579, 47, 95, 20);
			
		} catch (ParseException e) {}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == btnFeminino)
			if(btnFeminino.isSelected() == false)
				btnFeminino.setBorder(new LineBorder(Color.RED));
			else
				btnFeminino.setBorder(new LineBorder(Color.LIGHT_GRAY));
		else if(e.getSource() == btnMasculino)
			if(btnMasculino.isSelected() == false)
				btnMasculino.setBorder(new LineBorder(Color.RED));
			else
				btnMasculino.setBorder(new LineBorder(Color.LIGHT_GRAY));
	}
	
	@Override
	public void keyPressed(KeyEvent e) {}
	@Override
	public void keyReleased(KeyEvent e) {
		if(e.getSource() == txtNome)
			if(txtNome.getText().trim().length() == 0)
				txtNome.setBorder(new LineBorder(Color.RED));
			else
				txtNome.setBorder(new LineBorder(Color.LIGHT_GRAY));
		else if(e.getSource() == txtData)
			if(txtData.getText().trim().length() < 10)
				txtData.setBorder(new LineBorder(Color.RED));
			else
				txtData.setBorder(new LineBorder(Color.LIGHT_GRAY));
		else if(e.getSource() == txtCpf)
			if(txtCpf.getText().trim().length() < 14)
				txtCpf.setBorder(new LineBorder(Color.RED));
			else
				txtCpf.setBorder(new LineBorder(Color.LIGHT_GRAY));
	}
	@Override
	public void keyTyped(KeyEvent e) {}
	
	@Override
	public void limparCampos(){
		txtNome.setText("");
		txtData.setText("");
		btnFeminino.setSelected(false);
		btnMasculino.setSelected(false);
		txtCpf.setText("");
		repaint();
	}
	
	@Override
	public void removerObrigatoriedade(){
		for (JLabelObrigatoria l: lbl)
			remove(l);
		repaint();
	}

	/*public static void main(String[] args) {
		JFramePadrao a = new JFramePadrao();
		a.getContentPane().add(new JPanelPessoa()).setBounds(0, 0, 690, 85);
		a.setSize(700, 120);
		a.setLocationRelativeTo(null);
	}*/
	
}