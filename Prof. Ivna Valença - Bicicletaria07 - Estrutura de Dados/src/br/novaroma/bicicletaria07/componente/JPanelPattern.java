package br.novaroma.bicicletaria07.componente;

import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

public class JPanelPattern extends JPanel {

	private static final long serialVersionUID = 1L;

	public JPanelPattern() {
		super(null);
	}
	
	public JPanelPattern(String titulo) {
		super(null);
		setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), titulo, TitledBorder.LEADING, TitledBorder.TOP, null, null));
	}

}