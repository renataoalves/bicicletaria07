package br.novaroma.bicicletaria07.componente;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.*;

import br.novaroma.bicicletaria07.model.ClienteFisico;
import br.novaroma.bicicletaria07.model.ClienteJuridico;
import br.novaroma.bicicletaria07.model.Fornecedor;
import br.novaroma.bicicletaria07.model.Funcionario;
import br.novaroma.bicicletaria07.model.Produto;
import br.novaroma.bicicletaria07.model.Servico;
import br.novaroma.bicicletaria07.view.ViewBusca;
import br.novaroma.bicicletaria07.view.ViewCadastro;
import br.novaroma.bicicletaria07.view.ViewFAQ;
import br.novaroma.bicicletaria07.view.ViewServicoFila;
import br.novaroma.bicicletaria07.view.ViewServicoLista;
import br.novaroma.bicicletaria07.view.ViewSuporte;

public abstract class BarraDeMenu extends JFramePattern implements ActionListener {
	
	private static final long serialVersionUID = 1L;
	
	private JMenuBar menuBar;
	
	private JMenu mnArquivo;
	private JMenuItem sair;
	
	private JMenu mnRegistro, mnCadastrar, mnCliente, mnBuscar, mnBCliente;
	private JMenuItem clienteF, clienteJ, funcionario, fornecedor, produto,
						bClienteF, bClienteJ, bFuncionario, bFornecedor, bProduto, bServico;
	
	private JMenu mnServico;
	private JMenuItem fila, lista;
	
	private JMenu mnAjuda;
	private JMenuItem faq, suporte;
	
	public BarraDeMenu(){
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		mnArquivo = new JMenu("Arquivo");
		mnArquivo.setToolTipText("ALT + 1");
		menuBar.add(mnArquivo).setMnemonic(KeyEvent.VK_1);
		
					sair = new JMenuItem("Sair");
					mnArquivo.add(sair).addActionListener(this);
		
		mnRegistro = new JMenu("Registro");
		mnRegistro .setToolTipText("ALT + 2");
		menuBar.add(mnRegistro).setMnemonic(KeyEvent.VK_2);
		
					mnCadastrar = new JMenu("Cadastrar");
					mnRegistro.add(mnCadastrar);
		
								mnCliente = new JMenu("Cliente");
								mnCadastrar.add(mnCliente);
								
											clienteF = new JMenuItem("F�sico");
											mnCliente.add(clienteF).addActionListener(this);
											
											clienteJ = new JMenuItem("Jur�dico");
											mnCliente.add(clienteJ).addActionListener(this);
								
								funcionario = new JMenuItem("Funcion�rio");
								mnCadastrar.add(funcionario).addActionListener(this);
								
								fornecedor = new JMenuItem("Fornecedor");
								mnCadastrar.add(fornecedor).addActionListener(this);
								
								produto = new JMenuItem("Produto");
								mnCadastrar.add(produto).addActionListener(this);
								
					mnBuscar = new JMenu("Buscar");
					mnRegistro.add(mnBuscar);
					
								mnBCliente = new JMenu("Cliente");
								mnBuscar.add(mnBCliente);
								
											bClienteF = new JMenuItem("F�sico");
											mnBCliente.add(bClienteF).addActionListener(this);
											
											bClienteJ = new JMenuItem("Jur�dico");
											mnBCliente.add(bClienteJ).addActionListener(this);
								
								bFuncionario = new JMenuItem("Funcion�rio");
								mnBuscar.add(bFuncionario).addActionListener(this);
								
								bFornecedor = new JMenuItem("Fornecedor");
								mnBuscar.add(bFornecedor).addActionListener(this);
								
								bProduto = new JMenuItem("Produto");
								mnBuscar.add(bProduto).addActionListener(this);
								
								bServico = new JMenuItem("Servi�o");
								mnBuscar.add(bServico).addActionListener(this);
					
		mnServico = new JMenu("Servi�o");
		mnServico.setToolTipText("ALT + 3");
		menuBar.add(mnServico).setMnemonic(KeyEvent.VK_3);
		
								fila = new JMenuItem("Fila de Atendimento");
								mnServico.add(fila).addActionListener(this);
								
								lista = new JMenuItem("Em Atendimento");
								mnServico.add(lista).addActionListener(this);
		
		mnAjuda = new JMenu("Ajuda");
		mnAjuda.setToolTipText("ALT + 4");
		menuBar.add(mnAjuda).setMnemonic(KeyEvent.VK_4);
		
					faq = new JMenuItem("FAQ");
					mnAjuda.add(faq).addActionListener(this);
		
					suporte = new JMenuItem("Contatar Suporte");
					mnAjuda.add(suporte).addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent e) {
		
		JIFramePattern frame = null;
		
		if(e.getSource() == sair)
			System.exit(0);
		
		else if(e.getSource() == clienteF)
			frame = new ViewCadastro(ClienteFisico.class);
		else if(e.getSource() == clienteJ)
			frame = new ViewCadastro(ClienteJuridico.class);
		else if(e.getSource() == funcionario)
			frame = new ViewCadastro(Funcionario.class);
		else if(e.getSource() == fornecedor)
			frame = new ViewCadastro(Fornecedor.class);
		else if(e.getSource() == produto)
			frame = new ViewCadastro(Produto.class);
		
		else if(e.getSource() == bClienteF)
			frame = new ViewBusca("Busca por Cliente F�sico", ClienteFisico.class);
		else if(e.getSource() == bClienteJ)
			frame = new ViewBusca("Busca por Cliente Jur�dico", ClienteJuridico.class);
		else if(e.getSource() == bFuncionario)
			frame = new ViewBusca("Busca por Funcion�rio", Funcionario.class);
		else if(e.getSource() == bFornecedor)
			frame = new ViewBusca("Busca por Fornecedor", Fornecedor.class);
		else if(e.getSource() == bProduto)
			frame = new ViewBusca("Busca por Produto", Produto.class);
		else if(e.getSource() == bServico)
			frame = new ViewBusca("Busca por Servico", Servico.class);
		
		else if(e.getSource() == fila)
			frame = new ViewServicoFila();
		else if(e.getSource() == lista)
			frame = new ViewServicoLista();

		else if(e.getSource() == faq)
			frame = new ViewFAQ();
		else if(e.getSource() == suporte)
			frame = new ViewSuporte("email", "identificador");

		if(frame != null){
			add(frame).requestFocus();
			centralizaForm(frame);
		}
	}
	
	private void centralizaForm(JInternalFrame frame) {
        Dimension desktopSize = this.getSize();
        Dimension jInternalFrameSize = frame.getSize();
        frame.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,
                (desktopSize.height - jInternalFrameSize.height) / 2);
    }
	
}