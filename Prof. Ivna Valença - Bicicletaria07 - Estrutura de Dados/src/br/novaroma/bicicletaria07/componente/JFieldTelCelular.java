package br.novaroma.bicicletaria07.componente;

import java.text.ParseException;

import javax.swing.text.MaskFormatter;

public class JFieldTelCelular extends JFFieldPattern {
	
	private static final long serialVersionUID = 1L;

	public JFieldTelCelular() throws ParseException {
		super(getMascara());
	}
	
	public JFieldTelCelular(String cel) throws ParseException {
		super(getMascara());
		setText(cel);
	}
	
	private static MaskFormatter getMascara() throws ParseException{
		MaskFormatter masc = new MaskFormatter("(##)#####-####");
		masc.setPlaceholderCharacter(' ');
		return masc;
	}
	
}