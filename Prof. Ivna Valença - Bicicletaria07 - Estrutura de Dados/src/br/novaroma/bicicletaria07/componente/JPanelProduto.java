package br.novaroma.bicicletaria07.componente;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.ParseException;

import javax.swing.border.LineBorder;

import br.novaroma.bicicletaria07.enums.TipoProdutoEnum;
import br.novaroma.bicicletaria07.interfaces.ILimparCampos;
import br.novaroma.bicicletaria07.interfaces.IRemoverObrigatoriedade;

public class JPanelProduto extends JPanelPattern implements ActionListener, KeyListener, ILimparCampos, IRemoverObrigatoriedade {

	private static final long serialVersionUID = 1L;

	private JLabelObrigatoria[] lbl;
	private JFieldPadrao txtNome, txtFabricante, txtMarca;
	private JFieldMoney txtValor;
	private JFieldNumero txtQuant;
	private JAreaPattern txtDescricao;
	private JComboBoxTipos combo;

	public JPanelProduto(){
		super("Produto");
		try{
			lbl = new JLabelObrigatoria[6];
			
			lbl[0] = new JLabelObrigatoria();
			add(lbl[0]).setBounds(15, 25, 7, 14);

			add(new JLabelPattern("Nome: ")).setBounds(25, 25, 45, 14);

			txtNome = new JFieldPadrao();
			txtNome.setDocument(new DocumentUpperCase());
			txtNome.addKeyListener(this);
			add(txtNome).setBounds(67, 25, 400, 20);

			lbl[1] = new JLabelObrigatoria();
			add(lbl[1]).setBounds(15, 50, 7, 14);

			add(new JLabelPattern("Quantidade: ")).setBounds(25, 50, 80, 14);

			txtQuant = new JFieldNumero(5);
			txtQuant.addKeyListener(this);
			add(txtQuant).setBounds(100, 50, 45, 20);

			lbl[2] = new JLabelObrigatoria();
			add(lbl[2]).setBounds(15, 75, 7, 14);

			add(new JLabelPattern("Fabricante: ")).setBounds(26, 75, 80, 14);

			txtFabricante = new JFieldPadrao();
			txtFabricante.addKeyListener(this);
			add(txtFabricante).setBounds(94, 75, 160, 20);

			lbl[3] = new JLabelObrigatoria();
			add(lbl[3]).setBounds(260, 75, 7, 14);

			add(new JLabelPattern("Marca: ")).setBounds(270, 75, 80, 14);

			txtMarca = new JFieldPadrao();
			txtMarca.addKeyListener(this);
			add(txtMarca).setBounds(310, 75, 150, 20);

			lbl[4] = new JLabelObrigatoria();
			add(lbl[4]).setBounds(15, 100, 7, 14);

			add(new JLabelPattern("Valor (U.) R$: ")).setBounds(25, 100, 115, 14);

			txtValor = new JFieldMoney();
			txtValor.addKeyListener(this);
			add(txtValor).setBounds(110, 100, 115, 20);

			lbl[5] = new JLabelObrigatoria();
			add(lbl[5]).setBounds(230, 100, 7, 14);

			add(new JLabelPattern("Tipo do Produto: ")).setBounds(240, 100, 100, 14);

			combo = new JComboBoxTipos(TipoProdutoEnum.class);
			combo.addActionListener(this);
			add(combo).setBounds(337, 100, 130, 20);

			add(new JLabelPattern("Descri��o: ")).setBounds(25, 125, 65, 14);

			txtDescricao = new JAreaPattern();
			add(txtDescricao).setBounds(88, 125, 380, 70);

		} catch (ParseException e){}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == combo)
			if(combo.getSelectedIndex() == 0)
				combo.setBorder(new LineBorder(Color.RED));
			else
				combo.setBorder(new LineBorder(Color.LIGHT_GRAY));
	}
	
	@Override
	public void keyPressed(KeyEvent e) {}
	@Override
	public void keyReleased(KeyEvent e) {
		if(e.getSource() == txtNome)
			if(txtNome.getText().trim().length() == 0)
				txtNome.setBorder(new LineBorder(Color.RED));
			else
				txtNome.setBorder(new LineBorder(Color.LIGHT_GRAY));
		else if(e.getSource() == txtFabricante)
			if(txtFabricante.getText().trim().length() == 0)
				txtFabricante.setBorder(new LineBorder(Color.RED));
			else
				txtFabricante.setBorder(new LineBorder(Color.LIGHT_GRAY));
		else if(e.getSource() == txtMarca)
			if(txtMarca.getText().trim().length() == 0)
				txtMarca.setBorder(new LineBorder(Color.RED));
			else
				txtMarca.setBorder(new LineBorder(Color.LIGHT_GRAY));
		else if(e.getSource() == txtValor)
			if(txtValor.getText().trim().equals("0,00"))
				txtValor.setBorder(new LineBorder(Color.RED));
			else
				txtValor.setBorder(new LineBorder(Color.LIGHT_GRAY));
		else if(e.getSource() == txtQuant)
			if(txtQuant.getText().trim().length() == 0)
				txtQuant.setBorder(new LineBorder(Color.RED));
			else
				txtQuant.setBorder(new LineBorder(Color.LIGHT_GRAY));
	}
	@Override
	public void keyTyped(KeyEvent e) {}
	
	@Override
	public void limparCampos(){
		txtNome.setText("");
		txtFabricante.setText("");
		txtMarca.setText("");
		txtValor.setText("");
		txtQuant.setText("");
		txtDescricao.setText("");
		combo.setSelectedIndex(0);
		repaint();
	}
	
	@Override
	public void removerObrigatoriedade(){
		for (JLabelObrigatoria l: lbl)
			remove(l);
		repaint();
	}

//	public static void main(String[] args) {
//		JFramePadrao a = new JFramePadrao();
//		a.getContentPane().add(new JPanelProduto()).setBounds(0, 0, 480, 205);
//		a.setSize(490, 250);
//		a.setLocationRelativeTo(null);
//	}

}