package br.novaroma.bicicletaria07.componente;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import br.novaroma.bicicletaria07.enums.TipoFuncionarioEnum;
import br.novaroma.bicicletaria07.interfaces.ILimparCampos;
import br.novaroma.bicicletaria07.interfaces.IRemoverObrigatoriedade;
import br.novaroma.bicicletaria07.model.ClienteFisico;
import br.novaroma.bicicletaria07.model.Fornecedor;
import br.novaroma.bicicletaria07.model.Funcionario;
import br.novaroma.bicicletaria07.model.Produto;

public class JPanelCadastro extends JPanelPattern implements ActionListener, ILimparCampos, IRemoverObrigatoriedade {

	private static final long serialVersionUID = 1L;

	private JPanelPessoa panelInfoPessoais;
	private JPanelEmpresa panelInfoEmpresarias;
	private JPanelContato panelContato;
	private JPanelContatoFuncionario panelContatoF;
	private JPanelEndereco panelEndereco;
	private JPanelAcesso panelAcesso;
	private JPanelDefinicaoPapel panelDefinicao;
	private JPanelProdutoLista panelListaProduto;
	private JPanelProduto panelProduto;

	private JIFrameProduto panelNovoProduto;

	public JPanelCadastro(Class<?> tipo) {
		if(tipo == Produto.class){
			panelProduto = new JPanelProduto();
			add(panelProduto).setBounds(0, 0, 480, 208);
		} else {
			if(tipo == ClienteFisico.class || tipo == Funcionario.class){
				panelInfoPessoais = new JPanelPessoa();
				add(panelInfoPessoais).setBounds(5, 0, 680, 80);
			} else {
				panelInfoEmpresarias = new JPanelEmpresa();
				add(panelInfoEmpresarias).setBounds(5, 0, 680, 80);
			}

			if(tipo == Funcionario.class){
				panelContatoF = new JPanelContatoFuncionario();
				add(panelContatoF).setBounds(5, 85, 680, 78);
			}else {
				panelContato = new JPanelContato();
				add(panelContato).setBounds(5, 85, 680, 78);
			}

			panelEndereco = new JPanelEndereco();
			add(panelEndereco).setBounds(5, 170, 680, 140);

			if(tipo == Funcionario.class){
				panelDefinicao = new JPanelDefinicaoPapel("Tipo de Funcionário:", TipoFuncionarioEnum.class);
				add(panelDefinicao).setBounds(5, 315, 320, 75);

				panelAcesso = new JPanelAcesso();
				add(panelAcesso).setBounds(335, 315, 350, 75);
			} else if(tipo == Fornecedor.class){
				panelListaProduto = new JPanelProdutoLista();
				add(panelListaProduto).setBounds(5, 315, 680, 126);
				panelListaProduto.getBtn().addActionListener(this);
			}
		}
	}

	public void actionPerformed(ActionEvent e) {
		panelNovoProduto = new JIFrameProduto();
		add(panelNovoProduto).setBounds(105, 100, 495, 275);
	}

	@Override
	public void limparCampos() {
		if(panelInfoPessoais != null)
			panelInfoPessoais.limparCampos();
		if(panelInfoEmpresarias != null)
			panelInfoEmpresarias.limparCampos();
		if(panelContato != null)
			panelContato.limparCampos();
		if(panelContatoF != null)
			panelContatoF.limparCampos();
		if(panelEndereco != null)
			panelEndereco.limparCampos();
		if(panelAcesso != null)
			panelAcesso.limparCampos();
		if(panelDefinicao != null)
			panelDefinicao.limparCampos();
		if(panelListaProduto != null)
			panelListaProduto.limparCampos();
		if(panelProduto != null)
			panelProduto.limparCampos();
		repaint();
	}
	
	@Override
	public void removerObrigatoriedade() {
		if(panelInfoPessoais != null)
			panelInfoPessoais.removerObrigatoriedade();
		if(panelInfoEmpresarias != null)
			panelInfoEmpresarias.removerObrigatoriedade();
		if(panelContato != null)
			panelContato.removerObrigatoriedade();
		if(panelContatoF != null)
			panelContatoF.removerObrigatoriedade();
		if(panelEndereco != null)
			panelEndereco.removerObrigatoriedade();
		if(panelAcesso != null)
			panelAcesso.removerObrigatoriedade();
		if(panelDefinicao != null)
			panelDefinicao.removerObrigatoriedade();
		if(panelListaProduto != null)
			panelListaProduto.removerObrigatoriedade();
		if(panelProduto != null)
			panelProduto.removerObrigatoriedade();
		repaint();
	}

	/*public static void main(String[] args) {
		JFrame a = new JFrame();
		a.add(new JPanelCadastro(ClienteFisico.class));
		a.setVisible(true);
		a.setSize(800, 500);
		a.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		a.setLocationRelativeTo(null);
	}*/

}