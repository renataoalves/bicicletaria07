package br.novaroma.bicicletaria07.componente;

import java.text.ParseException;

import javax.swing.text.MaskFormatter;

public class JFieldCnpj extends JFFieldPattern {

	private static final long serialVersionUID = 1L;

	public JFieldCnpj() throws ParseException {
		super(getMascara());
	}
	
	public JFieldCnpj(String cnpj) throws ParseException {
		super(getMascara());
		setText(cnpj);
	}
	
	private static MaskFormatter getMascara() throws ParseException{
		MaskFormatter masc = new MaskFormatter("###.###.###/####-##");
		masc.setPlaceholderCharacter(' ');
		return masc;
	}
	
}