package br.novaroma.bicicletaria07.componente;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.ParseException;

import javax.swing.border.LineBorder;

import br.novaroma.bicicletaria07.interfaces.ILimparCampos;
import br.novaroma.bicicletaria07.interfaces.IRemoverObrigatoriedade;

public class JPanelEmpresa extends JPanelPattern implements KeyListener, ILimparCampos, IRemoverObrigatoriedade {

	private static final long serialVersionUID = 1L;
	
	private JLabelObrigatoria[] lbl;
	private JFieldCnpj txtCnpj;
	private JFieldPadrao txtRazao;
	
	public JPanelEmpresa(){
		super("Informa��es Empresariais");
		try{
			lbl = new JLabelObrigatoria[2];
			
			lbl[0] = new JLabelObrigatoria();
			add(lbl[0]).setBounds(14, 25, 7, 14);
			
			add(new JLabelPattern("Raz�o Social:")).setBounds(22, 24, 97, 14);
			
			txtRazao = new JFieldPadrao();
			txtRazao.setDocument(new DocumentUpperCase());
			txtRazao.addKeyListener(this);
			add(txtRazao).setBounds(103, 22, 571, 20);
			
			lbl[1] = new JLabelObrigatoria();
			add(lbl[1]).setBounds(58, 50, 7, 14);
			
			add(new JLabelPattern("CNPJ:")).setBounds(66, 50, 41, 14);
			
			txtCnpj = new JFieldCnpj();
			txtCnpj.addKeyListener(this);
			add(txtCnpj).setBounds(103, 50, 130, 20);
			
		} catch (ParseException e) {}
	}
	
	@Override
	public void keyPressed(KeyEvent e) {}
	@Override
	public void keyReleased(KeyEvent e) {
		if(e.getSource() == txtRazao)
			if(txtRazao.getText().trim().length() < 5)
				txtRazao.setBorder(new LineBorder(Color.RED));
			else
				txtRazao.setBorder(new LineBorder(Color.LIGHT_GRAY));
		else if(e.getSource() == txtCnpj)
			if(txtCnpj.getText().trim().length() < 19)
				txtCnpj.setBorder(new LineBorder(Color.RED));
			else
				txtCnpj.setBorder(new LineBorder(Color.LIGHT_GRAY));
	}
	@Override
	public void keyTyped(KeyEvent e) {}
	
	@Override
	public void limparCampos(){
		txtRazao.setText("");
		txtCnpj.setText("");
		repaint();
	}
	
	@Override
	public void removerObrigatoriedade(){
		for (JLabelObrigatoria l: lbl)
			remove(l);
		repaint();
	}
	
	/*public static void main(String[] args) {
		JFramePadrao a = new JFramePadrao();
		a.getContentPane().add(new JPanelEmpresa()).setBounds(0, 0, 690, 85);
		a.setSize(700, 120);
		a.setLocationRelativeTo(null);
	}*/
	
}