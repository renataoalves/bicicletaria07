package br.novaroma.bicicletaria07.componente;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JTextArea;

public class JAreaPattern extends JTextArea {

	private static final long serialVersionUID = 1L;

	public JAreaPattern(){
		criar();
	}
	
	public JAreaPattern(String string){
		super(string);
		criar();
	}
	
	public JAreaPattern(String string, Color color){
		super(string);
		setForeground(color);
		criar();
	}
	
	private void criar(){
		setLineWrap(true);
		setFont(new Font("Verdana", Font.PLAIN, 11));
	}
	
}