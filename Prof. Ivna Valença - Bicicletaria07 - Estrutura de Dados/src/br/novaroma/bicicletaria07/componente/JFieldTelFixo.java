package br.novaroma.bicicletaria07.componente;

import java.text.ParseException;

import javax.swing.text.MaskFormatter;

public class JFieldTelFixo extends JFFieldPattern {

	private static final long serialVersionUID = 1L;

	public JFieldTelFixo() throws ParseException {
		super(getMascara());
	}
	
	public JFieldTelFixo(String tel) throws ParseException {
		super(getMascara());
		setText(tel);
	}
	
	private static MaskFormatter getMascara() throws ParseException{
		MaskFormatter masc = new MaskFormatter("(##)####-####");
		masc.setPlaceholderCharacter(' ');
		return masc;
	}
	
}