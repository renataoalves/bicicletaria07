package br.novaroma.bicicletaria07.componente;

import java.text.ParseException;

import javax.swing.text.MaskFormatter;

public class JFieldData extends JFFieldPattern {

	private static final long serialVersionUID = 1L;

	public JFieldData() throws ParseException {
		super(getMascara());
	}
	
	public JFieldData(String data) throws ParseException {
		super(getMascara());
		setText(data);
	}
	
	private static MaskFormatter getMascara() throws ParseException{
		MaskFormatter masc = new MaskFormatter("##/##/####");
		masc.setPlaceholderCharacter(' ');
		return masc;
	}
	
}