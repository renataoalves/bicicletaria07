package br.novaroma.bicicletaria07.componente;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JComboBox;
import javax.swing.border.LineBorder;

import br.novaroma.bicicletaria07.interfaces.IRemoverObrigatoriedade;

public class JPanelSuporte extends JPanelPattern implements ActionListener, KeyListener, IRemoverObrigatoriedade {
	
	private static final long serialVersionUID = 1L;
	
	private JLabelObrigatoria[] lbl;
	private JComboBox<String> combo;
	private JAreaPattern txtDescricao;
	private JFieldPadrao txtEmail, txtDiretorio;
	private JButtonPattern btnAnexo, btnEnviar;
	
	public JPanelSuporte(String email, String identificador) {
		super("Especifica��o de Problema");
		
		lbl = new JLabelObrigatoria[2];
		
		lbl[0] = new JLabelObrigatoria();
		add(lbl[0]).setBounds(10, 20, 7, 14);
				
		add(new JLabelPattern("Informe-nos qual seu motivo de contata��o ao suporte:")).setBounds(20, 20, 354, 16);
		
		combo = new JComboBox<String>();
		combo.addActionListener(this);
		combo.addItem("-- SELECIONE PROBLEMA --");
		combo.addItem("ESQUECI LOGIN/SENHA");
		combo.addItem("LOGIN/SENHA N�O COINCIDEM");
		combo.addItem("M�DULO DE CADASTRO");
		combo.addItem("M�DULO DE ATENDIMENTO");
		combo.addItem("OUTRO");
		add(combo).setBounds(340, 20, 260, 20);
		
		lbl[1] = new JLabelObrigatoria();
		add(lbl[1]).setBounds(10, 50, 7, 14);
		
		add(new JLabelPattern("Email:")).setBounds(20, 50, 43, 16);
		
		txtEmail = new JFieldPadrao();
		txtEmail.addKeyListener(this);
		add(txtEmail).setBounds(60, 50, 350, 20);
		
		add(new JLabelPattern("Anexar Imagem:")).setBounds(20, 80, 110, 16);
		
		btnAnexo = new JButtonPattern("Anexar ");
		add(btnAnexo).setBounds(123, 78, 100, 25);
		
		txtDiretorio = new JFieldPadrao();
		add(txtDiretorio).setBounds(230, 80, 368, 20);
		
		add(new JLabelPattern("Descri��o:")).setBounds(20, 110, 67, 16);
		
		txtDescricao = new JAreaPattern();
		add(txtDescricao).setBounds(90, 110, 508, 250);

		btnEnviar = new JButtonPattern("Enviar");
		add(btnEnviar).setBounds(500, 363, 100, 25);
		
		setBounds(0, 0, 610, 400);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == combo)
			if(combo.getSelectedIndex() == 0)
				combo.setBorder(new LineBorder(Color.RED));
			else
				combo.setBorder(new LineBorder(Color.LIGHT_GRAY));
	}
	
	@Override
	public void keyPressed(KeyEvent e) {}
	@Override
	public void keyReleased(KeyEvent e) {
		if(e.getSource() == txtEmail)
			if(txtEmail.getText().trim().length() == 0 || !txtEmail.getText().contains("@"))
				txtEmail.setBorder(new LineBorder(Color.RED));
			else
				txtEmail.setBorder(new LineBorder(Color.LIGHT_GRAY));
	}
	@Override
	public void keyTyped(KeyEvent e) {}
	
	@Override
	public void removerObrigatoriedade(){
		for (JLabelObrigatoria l: lbl)
			remove(l);
		repaint();
	}
	
	public static void main(String[] args) {
		JFramePattern a = new JFramePattern();
		a.getContentPane().add(new JPanelSuporte("", "")).setBounds(0, 0, 610, 395);
		a.setSize(650, 430);
		a.setLocationRelativeTo(null);
	}

}