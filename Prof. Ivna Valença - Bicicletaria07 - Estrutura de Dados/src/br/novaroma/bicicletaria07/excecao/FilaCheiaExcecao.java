package br.novaroma.bicicletaria07.excecao;

import javax.swing.JOptionPane;

public class FilaCheiaExcecao extends Exception {

	private static final long serialVersionUID = 1L;

	public FilaCheiaExcecao(String tipo) {
		super();
		JOptionPane.showMessageDialog(null, "Fila de "+tipo+" est� cheia", "ERRO", 0);
	}
}
