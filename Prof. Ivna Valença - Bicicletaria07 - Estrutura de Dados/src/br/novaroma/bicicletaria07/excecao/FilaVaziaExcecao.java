package br.novaroma.bicicletaria07.excecao;

import javax.swing.JOptionPane;

public class FilaVaziaExcecao extends Exception {

	private static final long serialVersionUID = 1L;

	public FilaVaziaExcecao(String tipo) {
		super();
		JOptionPane.showMessageDialog(null, "A fila de "+tipo+" est� vazia", "NOTIFICA��O", 1);
	}
}
