package br.novaroma.bicicletaria07.excecao;

import javax.swing.JOptionPane;

public class NaoEncontradoExcecao extends Exception {
	
	private static final long serialVersionUID = 1L;

	public NaoEncontradoExcecao (String string){
		super(string);
		JOptionPane.showMessageDialog(null, string);
	}

}
